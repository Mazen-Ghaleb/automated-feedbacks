﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Word = Microsoft.Office.Interop.Word;
using Excel = Microsoft.Office.Interop.Excel;
using AutoFeedbacks.Properties;
using Microsoft.Office.Interop.Word;

namespace AutoFeedbacks
{
    public partial class MainForm : Form
    {
        string folderPath = "";
        String form1Path = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "form1.docx");
        String form2Path = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "form2.docx");
        String StudentPath;
        String InstructorPath;


      public MainForm(System.Drawing.Point Location)
        {
            this.Location = Location;
            InitializeComponent();
        }

        class InvalidBestofQuiz : Exception
        {
            public InvalidBestofQuiz() { }

            public InvalidBestofQuiz(string name)
                : base(String.Format(name))
            {

            }
        }

        //Find and Replace Method
        private void FindAndReplace(Word.Application wordApp, object ToFindText, object replaceWithText)
        {
            object matchCase = true;
            object matchWholeWord = true;
            object matchWildCards = false;
            object matchSoundLike = false;
            object nmatchAllforms = false;
            object forward = true;
            object format = false;
            object matchKashida = false;
            object matchDiactitics = false;
            object matchAlefHamza = false;
            object matchControl = false;
            object read_only = false;
            object visible = true;
            object replace = 2;
            object wrap = 1;

            wordApp.Selection.Find.Execute(ref ToFindText,
                ref matchCase, ref matchWholeWord,
                ref matchWildCards, ref matchSoundLike,
                ref nmatchAllforms, ref forward,
                ref wrap, ref format, ref replaceWithText,
                ref replace, ref matchKashida,
                ref matchDiactitics, ref matchAlefHamza,
                ref matchControl);
        }

        private void FindAndReplaceImage(Word.Application wordApp, String AltText, String PicPath)
        {
            object missing = Missing.Value;
            foreach (Microsoft.Office.Interop.Word.InlineShape s in wordApp.ActiveDocument.InlineShapes)
            {
                if (s.AlternativeText.Contains(AltText))
                {
                    InlineShape Snew = wordApp.ActiveDocument.InlineShapes.AddPicture(PicPath, ref missing,ref missing, s.Range);
                    Snew.Height = s.Height;
                    Snew.Width = s.Width;
                    s.Delete();
                }
            }
        }
        private void FindAndDeleteImage(Word.Application wordApp, String AltText)
        {
            object missing = Missing.Value;
            foreach (Microsoft.Office.Interop.Word.InlineShape s in wordApp.ActiveDocument.InlineShapes)
            {
                if (s.AlternativeText.Contains(AltText))
                {
                    s.Delete();
                }
            }
        }

        //Creeate the Doc Method
        private void CreateWordDocumentForm(object filename, object SaveAs1, object SaveAs2, int SaveAsType)
        {
            Word.Application wordApp = new Word.Application();
            object missing = Missing.Value;
            Word.Document myWordDoc = null;
            try
            {
                if (File.Exists((string)filename))
                {
                    object readOnly = false;
                    wordApp.Visible = false;

                    try
                    {
                        myWordDoc = wordApp.Documents.Open(ref filename, ref missing, ref readOnly,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing, ref missing);
                        myWordDoc.Activate();
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                        myWordDoc.Close(false, missing, missing);
                        wordApp.Quit(false, missing, missing);
                        return;
                    }

                    // calculate mark percentage
                    String StringASUMarkPercentage;
                    double ASUMarkPercentage;
                    double ASUMark;
                    double ASUMaxMark;
                    if (textBox6.Text != "" && textBox7.Text != "")
                    {
                        try
                        {
                            ASUMark = double.Parse(textBox6.Text);
                            ASUMaxMark = double.Parse(textBox7.Text);
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show(error.Message + "\n\n Try again after fixing the error in ASU mark.");
                            myWordDoc.Close(false, missing, missing);
                            wordApp.Quit(false, missing, missing);
                            return;
                        }
                        if (ASUMaxMark <= 0)
                        {
                            MessageBox.Show("Max Mark can't be 0 or negative");
                            myWordDoc.Close();
                            wordApp.Quit();
                            return;
                        }

                        ASUMarkPercentage = (ASUMark / ASUMaxMark) * 100;
                        ASUMarkPercentage = Math.Round(ASUMarkPercentage, 2);

                        if (checkBox5.Checked)
                            StringASUMarkPercentage = ASUMarkPercentage.ToString() + "%";
                        else
                            StringASUMarkPercentage = ASUMarkPercentage.ToString();

                    }
                    else if (textBox6.Text != "" && textBox7.Text == "")
                    {
                        MessageBox.Show("Max Mark must have value");
                        myWordDoc.Close();
                        wordApp.Quit();
                        return;
                    }
                    else
                    {
                        ASUMarkPercentage = 0;
                        StringASUMarkPercentage = "";
                    }
                    String StringUELMarkPercentage;
                    double UELMarkPercentage = 0;

                    //find and replace
                    this.FindAndReplace(wordApp, "<StudentName>", textBox1.Text);
                    this.FindAndReplace(wordApp, "<StudentID>", textBox2.Text);
                    this.FindAndReplace(wordApp, "<CCode>", comboBox12.Text);
                    this.FindAndReplace(wordApp, "<CName>", comboBox13.Text);
                    if (checkBox7.Checked) {
                        if (comboBox6.SelectedIndex == 0)
                        {
                            this.FindAndReplaceImage(wordApp, "<FirstSignature>", textBox5.Text);
                            this.FindAndReplace(wordApp, "<Signature1>", "");
                        }
                        if (comboBox6.SelectedIndex == 1)
                        {
                            this.FindAndDeleteImage(wordApp, "<FirstSignature>");
                            this.FindAndReplace(wordApp, "<Signature1>", textBox21.Text);
                        }
                    }
                    else
                    {
                        this.FindAndDeleteImage(wordApp, "<FirstSignature>");
                        this.FindAndReplace(wordApp, "<Signature1>", "");
                    }
                    if (checkBox8.Checked)
                    {
                        if (comboBox7.SelectedIndex == 0)
                        {
                            this.FindAndReplaceImage(wordApp, "<SecondSignature>", textBox16.Text);
                            this.FindAndReplace(wordApp, "<Signature2>", "");
                        }
                        if (comboBox7.SelectedIndex == 1)
                        {
                            this.FindAndDeleteImage(wordApp, "<SecondSignature>");
                            this.FindAndReplace(wordApp, "<Signature2>", textBox22.Text);
                        }
                    }
                    else
                    {
                        this.FindAndDeleteImage(wordApp, "<SecondSignature>");
                        this.FindAndReplace(wordApp, "<Signature2>", "");
                    }
                    if (comboBox1.SelectedIndex != 10)
                    {
                        if (comboBox1.SelectedIndex == -1)
                            this.FindAndReplace(wordApp, "<Criteria>", "");
                        else
                            this.FindAndReplace(wordApp, "<Criteria>", comboBox1.SelectedItem.ToString());
                    }
                    else
                    {
                        this.FindAndReplace(wordApp, "<Criteria>", textBox9.Text);
                    }
                    ;
                    this.FindAndReplace(wordApp, "<Mark>", StringASUMarkPercentage);
                    if (checkBox2.Checked == true)
                    {
                        if (comboBox3.SelectedIndex == 0)
                            this.FindAndReplace(wordApp, "<Date>", dateTimePicker1.Value.ToString("dd/MM/yyyy"));
                        if (comboBox3.SelectedIndex == 1)
                            this.FindAndReplace(wordApp, "<Date>", comboBox4.SelectedItem.ToString());
                    }
                    else
                    {
                        this.FindAndReplace(wordApp, "<Date>", "");
                    }


                    if (comboBox1.SelectedIndex != 6 && comboBox1.SelectedIndex != 7 && comboBox2.SelectedIndex != 9 && comboBox2.SelectedIndex != 1)
                    {
                        switch (ASUMarkPercentage)
                        {
                            case double expression when expression >= 97:
                                this.FindAndReplace(wordApp, "<c1>", "✔");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 100)
                                {
                                    this.FindAndReplace(wordApp, "<b1>", "✔");
                                    this.FindAndReplace(wordApp, "<b2>", "");

                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b1>", "");
                                    this.FindAndReplace(wordApp, "<b2>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (200.0 / 3);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 93):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "✔");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");
                                if (expression >= 96)
                                {
                                    this.FindAndReplace(wordApp, "<b2>", "✔");
                                    this.FindAndReplace(wordApp, "<b3>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b2>", "");
                                    this.FindAndReplace(wordApp, "<b3>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (3.25) * ASUMarkPercentage - (220.25);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 89):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "✔");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");
                                if (expression >= 90)
                                {
                                    this.FindAndReplace(wordApp, "<b4>", "✔");
                                    this.FindAndReplace(wordApp, "<b5>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b4>", "");
                                    this.FindAndReplace(wordApp, "<b5>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 3.0 * ASUMarkPercentage - 197;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);
                                break;
                            case double expression when (expression >= 84):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "✔");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "✔");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");


                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 0.8 * ASUMarkPercentage - 1.2;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);
                                break;
                            case double expression when (expression >= 80):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "✔");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "✔");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 76):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "✔");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");
                                if (expression >= 79)
                                {
                                    this.FindAndReplace(wordApp, "<b7>", "✔");
                                    this.FindAndReplace(wordApp, "<b8>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b7>", "");
                                    this.FindAndReplace(wordApp, "<b8>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 73):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "✔");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");
                                if (expression >= 75)
                                {
                                    this.FindAndReplace(wordApp, "<b8>", "✔");
                                    this.FindAndReplace(wordApp, "<b9>", "");
                                    this.FindAndReplace(wordApp, "<b10>", "");
                                }
                                else if (expression >= 74)
                                {
                                    this.FindAndReplace(wordApp, "<b8>", "");
                                    this.FindAndReplace(wordApp, "<b9>", "✔");
                                    this.FindAndReplace(wordApp, "<b10>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b8>", "");
                                    this.FindAndReplace(wordApp, "<b9>", "");
                                    this.FindAndReplace(wordApp, "<b10>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (4.0 / 3) * ASUMarkPercentage - (124.0 / 3);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 70):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "✔");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "✔");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 67):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "✔");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");
                                if (expression >= 69)
                                {
                                    this.FindAndReplace(wordApp, "<b10>", "✔");
                                    this.FindAndReplace(wordApp, "<b11>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b10>", "");
                                    this.FindAndReplace(wordApp, "<b11>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 64):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "✔");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "✔");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (185.0 / 3);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 60):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "✔");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "✔");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 1.25 * ASUMarkPercentage - 35;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression < 60):

                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");

                                StringUELMarkPercentage = " ";

                                if (textBox6.Text != "" && textBox7.Text != "")
                                {
                                    this.FindAndReplace(wordApp, "<c12>", "✔");
                                    if (expression >= 59)
                                    {
                                        this.FindAndReplace(wordApp, "<b13>", "✔");
                                        this.FindAndReplace(wordApp, "<b14>", "");
                                        this.FindAndReplace(wordApp, "<b15>", "");
                                        this.FindAndReplace(wordApp, "<b16>", "");
                                    }
                                    else if (expression >= 40)
                                    {
                                        this.FindAndReplace(wordApp, "<b13>", "");
                                        this.FindAndReplace(wordApp, "<b14>", "✔");
                                        this.FindAndReplace(wordApp, "<b15>", "");
                                        this.FindAndReplace(wordApp, "<b16>", "");
                                    }
                                    else if (expression >= 20)
                                    {
                                        this.FindAndReplace(wordApp, "<b13>", "");
                                        this.FindAndReplace(wordApp, "<b14>", "");
                                        this.FindAndReplace(wordApp, "<b15>", "✔");
                                        this.FindAndReplace(wordApp, "<b16>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(wordApp, "<b13>", "");
                                        this.FindAndReplace(wordApp, "<b14>", "");
                                        this.FindAndReplace(wordApp, "<b15>", "");
                                        this.FindAndReplace(wordApp, "<b16>", "✔");
                                    }

                                    if (checkBox1.Checked == true)
                                    {
                                        UELMarkPercentage = (2.0 / 3) * ASUMarkPercentage;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<c12>", "");
                                    this.FindAndReplace(wordApp, "<b13>", "");
                                    this.FindAndReplace(wordApp, "<b14>", "");
                                    this.FindAndReplace(wordApp, "<b15>", "");
                                    this.FindAndReplace(wordApp, "<b16>", "");
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);


                                break;
                            default:
                                StringUELMarkPercentage = " ";
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);
                                MessageBox.Show("ERROR 404");
                                myWordDoc.Close(false, missing, missing);
                                wordApp.Quit(false, missing, missing);
                                return;
                        }
                    }
                    else
                    {
                        switch (ASUMarkPercentage)
                        {
                            case double expression when expression >= 97:
                                this.FindAndReplace(wordApp, "<c1>", "✔");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 100)
                                {
                                    this.FindAndReplace(wordApp, "<b1>", "✔");
                                    this.FindAndReplace(wordApp, "<b2>", "");

                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b1>", "");
                                    this.FindAndReplace(wordApp, "<b2>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (200.0 / 3);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 93):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "✔");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");
                                if (expression >= 96)
                                {
                                    this.FindAndReplace(wordApp, "<b2>", "✔");
                                    this.FindAndReplace(wordApp, "<b3>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b2>", "");
                                    this.FindAndReplace(wordApp, "<b3>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (3.25) * ASUMarkPercentage - (220.25);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 89):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "✔");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");
                                if (expression >= 92)
                                {
                                    this.FindAndReplace(wordApp, "<b3>", "✔");
                                    this.FindAndReplace(wordApp, "<b4>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b3>", "");
                                    this.FindAndReplace(wordApp, "<b4>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 3.0 * ASUMarkPercentage - 197;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);
                                break;
                            case double expression when (expression >= 84):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "✔");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 88)
                                {
                                    this.FindAndReplace(wordApp, "<b5>", "✔");
                                    this.FindAndReplace(wordApp, "<b6>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b5>", "");
                                    this.FindAndReplace(wordApp, "<b6>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");



                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 0.8 * ASUMarkPercentage - 1.2;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);
                                break;
                            case double expression when (expression >= 80):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "✔");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "✔");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 76):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "✔");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "✔");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 73):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "✔");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 75)
                                {
                                    this.FindAndReplace(wordApp, "<b9>", "✔");
                                    this.FindAndReplace(wordApp, "<b10>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b9>", "");
                                    this.FindAndReplace(wordApp, "<b10>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (4.0 / 3) * ASUMarkPercentage - (124.0 / 3);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 70):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "✔");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 72)
                                {
                                    this.FindAndReplace(wordApp, "<b10>", "✔");
                                    this.FindAndReplace(wordApp, "<b11>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b10>", "");
                                    this.FindAndReplace(wordApp, "<b11>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 67):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "✔");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 69)
                                {
                                    this.FindAndReplace(wordApp, "<b11>", "✔");
                                    this.FindAndReplace(wordApp, "<b12>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b11>", "");
                                    this.FindAndReplace(wordApp, "<b12>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 64):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "✔");
                                this.FindAndReplace(wordApp, "<c11>", "");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 66)
                                {
                                    this.FindAndReplace(wordApp, "<b13>", "✔");
                                    this.FindAndReplace(wordApp, "<b14>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b13>", "");
                                    this.FindAndReplace(wordApp, "<b14>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (185.0 / 3);
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression >= 60):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "✔");
                                this.FindAndReplace(wordApp, "<c12>", "");

                                if (expression >= 62)
                                {
                                    this.FindAndReplace(wordApp, "<b15>", "✔");
                                    this.FindAndReplace(wordApp, "<b16>", "");
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<b15>", "");
                                    this.FindAndReplace(wordApp, "<b16>", "✔");
                                }
                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b17>", "");
                                this.FindAndReplace(wordApp, "<b18>", "");
                                this.FindAndReplace(wordApp, "<b19>", "");
                                this.FindAndReplace(wordApp, "<b20>", "");

                                if (checkBox1.Checked == true)
                                {
                                    UELMarkPercentage = 1.25 * ASUMarkPercentage - 35;
                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                    if (checkBox5.Checked)
                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                    else
                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                }
                                else
                                {
                                    StringUELMarkPercentage = " ";
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            case double expression when (expression < 60):
                                this.FindAndReplace(wordApp, "<c1>", "");
                                this.FindAndReplace(wordApp, "<c2>", "");
                                this.FindAndReplace(wordApp, "<c3>", "");
                                this.FindAndReplace(wordApp, "<c4>", "");
                                this.FindAndReplace(wordApp, "<c5>", "");
                                this.FindAndReplace(wordApp, "<c6>", "");
                                this.FindAndReplace(wordApp, "<c7>", "");
                                this.FindAndReplace(wordApp, "<c8>", "");
                                this.FindAndReplace(wordApp, "<c9>", "");
                                this.FindAndReplace(wordApp, "<c10>", "");
                                this.FindAndReplace(wordApp, "<c11>", "");

                                this.FindAndReplace(wordApp, "<b1>", "");
                                this.FindAndReplace(wordApp, "<b2>", "");
                                this.FindAndReplace(wordApp, "<b3>", "");
                                this.FindAndReplace(wordApp, "<b4>", "");
                                this.FindAndReplace(wordApp, "<b5>", "");
                                this.FindAndReplace(wordApp, "<b6>", "");
                                this.FindAndReplace(wordApp, "<b7>", "");
                                this.FindAndReplace(wordApp, "<b8>", "");
                                this.FindAndReplace(wordApp, "<b9>", "");
                                this.FindAndReplace(wordApp, "<b10>", "");
                                this.FindAndReplace(wordApp, "<b11>", "");
                                this.FindAndReplace(wordApp, "<b12>", "");
                                this.FindAndReplace(wordApp, "<b13>", "");
                                this.FindAndReplace(wordApp, "<b14>", "");
                                this.FindAndReplace(wordApp, "<b15>", "");
                                this.FindAndReplace(wordApp, "<b16>", "");

                                StringUELMarkPercentage = " ";

                                if (textBox6.Text != "" && textBox7.Text != "")
                                {
                                    this.FindAndReplace(wordApp, "<c12>", "✔");
                                    if (expression >= 59)
                                    {
                                        this.FindAndReplace(wordApp, "<b17>", "✔");
                                        this.FindAndReplace(wordApp, "<b18>", "");
                                        this.FindAndReplace(wordApp, "<b19>", "");
                                        this.FindAndReplace(wordApp, "<b20>", "");
                                    }
                                    else if (expression >= 40)
                                    {
                                        this.FindAndReplace(wordApp, "<b17>", "");
                                        this.FindAndReplace(wordApp, "<b18>", "✔");
                                        this.FindAndReplace(wordApp, "<b19>", "");
                                        this.FindAndReplace(wordApp, "<b20>", "");
                                    }
                                    else if (expression >= 20)
                                    {
                                        this.FindAndReplace(wordApp, "<b17>", "");
                                        this.FindAndReplace(wordApp, "<b18>", "");
                                        this.FindAndReplace(wordApp, "<b19>", "✔");
                                        this.FindAndReplace(wordApp, "<b20>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(wordApp, "<b17>", "");
                                        this.FindAndReplace(wordApp, "<b18>", "");
                                        this.FindAndReplace(wordApp, "<b19>", "");
                                        this.FindAndReplace(wordApp, "<b20>", "✔");
                                    }
                                    if (checkBox1.Checked == true)
                                    {
                                        UELMarkPercentage = (2.0 / 3) * ASUMarkPercentage;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                }
                                else
                                {
                                    this.FindAndReplace(wordApp, "<c12>", "");
                                    this.FindAndReplace(wordApp, "<b17>", "");
                                    this.FindAndReplace(wordApp, "<b18>", "");
                                    this.FindAndReplace(wordApp, "<b19>", "");
                                    this.FindAndReplace(wordApp, "<b20>", "");
                                }
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);

                                break;
                            default:
                                StringUELMarkPercentage = " ";
                                this.FindAndReplace(wordApp, "<UELMark>", StringUELMarkPercentage);
                                MessageBox.Show("ERROR 404");
                                myWordDoc.Close(false, missing, missing);
                                wordApp.Quit(false, missing, missing);
                                return;

                        }
                    }

                }
                else
                {
                    MessageBox.Show("File not Found!");
                    myWordDoc.Close(false, missing, missing);
                    wordApp.Quit(false, missing, missing);
                    return;
                }
                //Save as
                try
                {
                    string fileNameOnly1 = SaveAs1.ToString().TrimEnd('.', 'd', 'o', 'c', 'x');
                    string fileNameOnly2 = SaveAs2.ToString().TrimEnd('.', 'p', 'd', 'f');
                    int count1 = 1;
                    int count2 = 1;
                    while ((File.Exists(Path.Combine(SaveAs1.ToString()))))
                    {
                        string tempFileName1 = string.Format("{0}({1})", fileNameOnly1, count1++);
                        SaveAs1 = tempFileName1 + ".docx";
                    }
                    while ((File.Exists(Path.Combine(SaveAs2.ToString()))))
                    {
                        string tempFileName2 = string.Format("{0}({1})", fileNameOnly2, count2++);
                        SaveAs2 = tempFileName2 + ".pdf";
                    }
                    if (SaveAsType == 1)
                    {
                        myWordDoc.SaveAs2(ref SaveAs1, ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);
                        MessageBox.Show("File Created!");
                        myWordDoc.Close();
                        wordApp.Quit();
                    }
                    else if (SaveAsType == 2)
                    {
                        myWordDoc.SaveAs2(ref SaveAs1, ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);

                        myWordDoc.SaveAs2(ref SaveAs2, Word.WdSaveFormat.wdFormatPDF, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);
                        myWordDoc.Close();
                        wordApp.Quit();

                        if (File.Exists(Path.Combine(SaveAs1.ToString())))
                        {
                            File.Delete(Path.Combine(SaveAs1.ToString()));
                        }
                        MessageBox.Show("File Created!");
                    }
                    else if (SaveAsType == 3)
                    {
                        myWordDoc.SaveAs2(ref SaveAs1, ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);

                        myWordDoc.SaveAs2(ref SaveAs2, Word.WdSaveFormat.wdFormatPDF, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);
                        MessageBox.Show("File Created!");
                        myWordDoc.Close();
                        wordApp.Quit();
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                    myWordDoc.Close(false, missing, missing);
                    wordApp.Quit(false, missing, missing);
                    return;
                }
            }
            catch
            {
                try
                {
                    myWordDoc.Close(false, missing, missing);
                    wordApp.Quit(false, missing, missing);
                    return;
                }
                catch { }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox7.Checked && comboBox6.SelectedIndex == 0 && textBox5.Text == "" || checkBox8.Checked && comboBox7.SelectedIndex == 0 && textBox16.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }

            String formPath;
            if (comboBox1.SelectedIndex != 6 && comboBox1.SelectedIndex != 7 && comboBox2.SelectedIndex != 9 && comboBox2.SelectedIndex != 1)
                formPath = form1Path;
            else
                formPath = form2Path;
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            
            if (comboBox1.SelectedIndex != 10)
            {
                String filename = (comboBox1.SelectedIndex == -1) ? textBox2.Text + " - " + " Feedback" : textBox2.Text + " - " + comboBox1.SelectedItem.ToString() + " Feedback";
                try
                {
                    CreateWordDocumentForm(formPath, textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 1);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                }
            }
            else
            {
                String filename = (comboBox1.SelectedItem.ToString() == "") ? textBox2.Text : textBox2.Text + " - " + textBox9.Text + " Feedback";
                try
                {
                    CreateWordDocumentForm(formPath, textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 1);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                }

            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if (checkBox7.Checked && comboBox6.SelectedIndex == 0 && textBox5.Text == "" || checkBox8.Checked && comboBox7.SelectedIndex == 0 && textBox16.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }
            String formPath;
            if (comboBox1.SelectedIndex != 6 && comboBox1.SelectedIndex != 7 && comboBox2.SelectedIndex != 9 && comboBox2.SelectedIndex != 1)
                formPath = form1Path;
            else
                formPath = form2Path;
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            if (comboBox1.SelectedIndex != 10)
            {
                String filename = (comboBox1.SelectedIndex == -1) ? textBox2.Text + " - " + " Feedback" : textBox2.Text + " - " + comboBox1.SelectedItem.ToString() + " Feedback";
                try
                {
                    CreateWordDocumentForm(formPath, textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 2);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                }

            }
            else
            {
                String filename = (comboBox1.SelectedItem.ToString() == "") ? textBox2.Text : textBox2.Text + " - " + textBox9.Text + " Feedback";
                try
                {
                    CreateWordDocumentForm(formPath, textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 2);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                }
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (checkBox7.Checked && comboBox6.SelectedIndex == 0 && textBox5.Text == "" || checkBox8.Checked && comboBox7.SelectedIndex == 0 && textBox16.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }

            String formPath;
            if (comboBox1.SelectedIndex != 6 && comboBox1.SelectedIndex != 7 && comboBox2.SelectedIndex != 9 && comboBox2.SelectedIndex != 1)
                formPath = form1Path;
            else
                formPath = form2Path;
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            if (comboBox1.SelectedIndex != 10)
            {
                String filename = (comboBox1.SelectedIndex == -1) ? textBox2.Text + " - " + " Feedback" : textBox2.Text + " - " + comboBox1.SelectedItem.ToString() + " Feedback";
                try
                {
                    CreateWordDocumentForm(formPath, textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 3);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                }
            }
            else
            {
                String filename = (comboBox1.SelectedItem.ToString() == "") ? textBox2.Text : textBox2.Text + " - " + textBox9.Text + " Feedback";
                try
                {
                    CreateWordDocumentForm(formPath, textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 3);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                }
            }
        }
        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox2.Checked)
            {
                comboBox3.SelectedIndex = 0;
                comboBox3.Enabled = true;
                comboBox3.Visible = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Visible = false;
                comboBox3.Enabled = false;
                comboBox3.Visible = false;
                comboBox4.Enabled = false;
                comboBox4.Visible = false;
            }
            if (comboBox3.SelectedIndex == 0 && checkBox2.Checked)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker1.Visible = true;
                comboBox4.Enabled = false;
                comboBox4.Visible = false;
            }
            else if (comboBox3.SelectedIndex == 1 && checkBox2.Checked)
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Visible = false;
                comboBox4.SelectedIndex = 0;
                comboBox4.Enabled = true;
                comboBox4.Visible = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Visible = false;
                comboBox4.Enabled = false;
                comboBox4.Visible = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ChangePath(textBox8);
        }

        private void ChangePath(TextBox TheTextBox)
        {
            FolderBrowserDialog folderBrowserDialog1 = new FolderBrowserDialog();
            if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                folderPath = folderBrowserDialog1.SelectedPath;
                TheTextBox.Text = folderPath;
            }
            
        }

        private void ChangePath2(TextBox TheTextBox)
        {
            OpenFileDialog FileDialog1 = new OpenFileDialog();
            FileDialog1.Filter = "Image Files(*.jpg; *.jpeg)|*.jpg; *.jpeg";
            if (FileDialog1.ShowDialog() == DialogResult.OK)
            {
                TheTextBox.Text = FileDialog1.FileName;
            }  
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex != 10)
            {
                label11.Visible = false;
                label11.Enabled = false;
                textBox9.Enabled = false;
                textBox9.Visible = false;
                label12.Visible = false;
                label12.Enabled = false;
                comboBox2.Visible = false;
                comboBox2.Enabled = false;
            }
            else
            {
                label11.Visible = true;
                label11.Enabled = true;
                textBox9.Enabled = true;
                textBox9.Visible = true;
                label12.Visible = true;
                label12.Enabled = true;
                comboBox2.SelectedIndex = 0;
                comboBox2.Visible = true;
                comboBox2.Enabled = true;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox10.Text = textBox1.Text = Settings.Default["Name"].ToString();
            textBox11.Text = textBox2.Text = Settings.Default["ID"].ToString();
            comboBox15.SelectedIndex = comboBox12.SelectedIndex = comboBox17.SelectedIndex = 
                comboBox14.SelectedIndex =  comboBox13.SelectedIndex = comboBox16.SelectedIndex = Settings.Default.CourseIndex;
            comboBox5.SelectedIndex = 0;
            checkBox5.Checked = Settings.Default.PercentageSymbol;
            checkBox6.Checked = Settings.Default.BestOfQuizzes;
            textBox8.Text = Settings.Default["Path"].ToString();
            ExtractEmbeddedFile("AutoFeedbacks.form1.docx", form1Path);
            ExtractEmbeddedFile("AutoFeedbacks.form2.docx", form2Path);
            if (Settings.Default["StudentPath"].ToString() == "" || !Settings.Default["StudentPath"].ToString().EndsWith("Student.xlsx"))
                StudentPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Student.xlsx");
            else
                StudentPath = Settings.Default["StudentPath"].ToString();
            if (Settings.Default["InstructorPath"].ToString() == "" || !Settings.Default["InstructorPath"].ToString().EndsWith("Instructor.xlsx"))
                InstructorPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Instructor.xlsx");
            else
                InstructorPath = Settings.Default["InstructorPath"].ToString();

            if (!(File.Exists(StudentPath)))
            {
                StudentPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Student.xlsx");
                ExtractEmbeddedFile("AutoFeedbacks.Student.xlsx", StudentPath);
            }

            if (!(File.Exists(InstructorPath)))
            {
                InstructorPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Instructor.xlsx");
                ExtractEmbeddedFile("AutoFeedbacks.Instructor.xlsx", InstructorPath);
            }
            textBox27.Text = StudentPath;
            textBox28.Text = InstructorPath;
            // Checking if user has Excel and Word
            Type officeType1 = Type.GetTypeFromProgID("Word.Application");
            Type officeType2 = Type.GetTypeFromProgID("Excel.Application");
            
            if (officeType1 != null && officeType2 == null)
            {
                MessageBox.Show("Please install Excel for the excel functions to work and restart the application.");
                //no Excel installed
            }
            else if (officeType1 == null)
            {
                MessageBox.Show("Please install Microsoft Word for the application to work.");
                System.Windows.Forms.Application.Exit();
                //no Word installed
            }
            else if (officeType1 != null && officeType2 != null)
            {
                //Word and Excel installed
            }
        }

        private void Name_TextChanged(object sender, EventArgs e)
        {
            if (Settings.Default["Name"].ToString() != textBox1.Text)
                Settings.Default["Name"] = textBox10.Text = textBox1.Text;
            else
                Settings.Default["Name"] = textBox1.Text = textBox10.Text;
            Settings.Default.Save();
        }

        private void ID_TextChanged(object sender, EventArgs e)
        {
            if (Settings.Default["ID"].ToString() != textBox2.Text)
                Settings.Default["ID"] = textBox11.Text = textBox2.Text;
            else
                Settings.Default["ID"] = textBox2.Text = textBox11.Text;
            Settings.Default.Save();
        }

        private void Course_Changed(object sender, EventArgs e)
        {
            if (Settings.Default.CourseIndex != comboBox12.SelectedIndex)
                Settings.Default.CourseIndex = comboBox15.SelectedIndex  = comboBox17.SelectedIndex =
                comboBox14.SelectedIndex = comboBox13.SelectedIndex = comboBox16.SelectedIndex = comboBox12.SelectedIndex;
            else if (Settings.Default.CourseIndex != comboBox15.SelectedIndex)
                Settings.Default.CourseIndex  = comboBox17.SelectedIndex = comboBox14.SelectedIndex = 
                    comboBox13.SelectedIndex = comboBox16.SelectedIndex = comboBox12.SelectedIndex = comboBox15.SelectedIndex;
            else if (Settings.Default.CourseIndex != comboBox17.SelectedIndex)
                Settings.Default.CourseIndex = comboBox14.SelectedIndex = comboBox13.SelectedIndex = 
                    comboBox16.SelectedIndex = comboBox12.SelectedIndex = comboBox15.SelectedIndex = comboBox17.SelectedIndex;
            else if (Settings.Default.CourseIndex != comboBox13.SelectedIndex)
                Settings.Default.CourseIndex = comboBox14.SelectedIndex = comboBox16.SelectedIndex = 
                    comboBox12.SelectedIndex = comboBox15.SelectedIndex = comboBox17.SelectedIndex = comboBox13.SelectedIndex;
            else if (Settings.Default.CourseIndex != comboBox14.SelectedIndex)
                Settings.Default.CourseIndex = comboBox13.SelectedIndex = comboBox16.SelectedIndex =
                    comboBox12.SelectedIndex = comboBox15.SelectedIndex = comboBox17.SelectedIndex = comboBox14.SelectedIndex;
            else
                Settings.Default.CourseIndex = comboBox13.SelectedIndex = comboBox14.SelectedIndex =
                    comboBox12.SelectedIndex = comboBox15.SelectedIndex = comboBox17.SelectedIndex = comboBox16.SelectedIndex;
            Settings.Default.Save();
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            Settings.Default["Path"] = textBox8.Text;
            Settings.Default.Save();
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.SelectedIndex == 0 && checkBox2.Checked)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker1.Visible = true;
                comboBox4.Enabled = false;
                comboBox4.Visible = false;
            }
            else if (comboBox3.SelectedIndex == 1 && checkBox2.Checked)
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Visible = false;
                comboBox4.SelectedIndex = 0;
                comboBox4.Enabled = true;
                comboBox4.Visible = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Visible = false;
                comboBox4.Enabled = false;
                comboBox4.Visible = false;
            }

        }
        private void button5_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
            textBox7.Text = "";
            textBox9.Text = "";
            checkBox2.Checked = false;
            comboBox1.SelectedIndex = -1;
            comboBox2.SelectedIndex = -1;
            comboBox3.SelectedIndex = -1;
            comboBox4.SelectedIndex = -1;
        }

        private void RadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            if (rb.Checked)
                foreach (TabPage tab in tabControl1.TabPages)
                    if (tab.Text.Trim() == rb.Text.Trim())
                    {
                        tabControl1.SelectedTab = tab;
                        Label0.Text = rb.Text.ToUpper();
                        break;
                    }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            String filename = StudentPath;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = null;
            Excel.Worksheet xlWorkSheet;
            object missing = System.Reflection.Missing.Value;

            if (!File.Exists((string)filename))
            {
                StudentPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Student.xlsx");
                ExtractEmbeddedFile("AutoFeedbacks.Student.xlsx", StudentPath);
                filename = textBox27.Text = StudentPath;
            }

            if (File.Exists((string)filename))
            {
                object readOnly = false;
                xlApp.Visible = true;

                try
                {
                    xlWorkBook = xlApp.Workbooks.Open(filename, missing, readOnly,
                                            missing, missing, missing,
                                            missing, missing, missing,
                                            missing, missing, missing,
                                            missing, missing, missing);
                    xlWorkBook.Activate();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                    xlWorkBook.Close(false, missing, missing);
                    xlApp.Quit();
                    return;
                }
            }
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
        }

        private void CreateWordDocumentExcel(object SaveAs1, object SaveAs2, int SaveAsType)
        {
            bool isFirstTime = true;
            object missing = Missing.Value;

            Word.Application wordApp = new Word.Application();
            Word.Document myWordDoc = null;

            String filename2 = StudentPath;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = null;
            Excel.Worksheet xlWorkSheet;
            try
            {
                if (!File.Exists((string)filename2))
                {
                        StudentPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Student.xlsx");
                        ExtractEmbeddedFile("AutoFeedbacks.Student.xlsx", StudentPath);
                        filename2 = textBox27.Text = StudentPath;
                }

                if (File.Exists((string)filename2))
                {
                    object readOnly2 = false;
                    xlApp.Visible = false;

                    try
                    {
                        xlWorkBook = xlApp.Workbooks.Open(filename2, missing, readOnly2,
                                                missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing);
                        xlWorkBook.Activate();
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                        xlWorkBook.Close(false, missing, missing);
                        myWordDoc.Close(false, missing, missing);
                        wordApp.Quit(false, missing, missing);
                        xlApp.Quit();
                        return;
                    }
                }

                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                if (checkBox6.Checked){
                    if (!(xlWorkSheet.Cells[2, 3].Value2.ToString() == "True" && xlWorkSheet.Cells[2, 4].Value2.ToString() == "True" && xlWorkSheet.Cells[2, 5].Value2.ToString() == "True"))
                    {
                        try
                        {
                            xlWorkBook.Close(false, missing, missing);
                            xlApp.Quit();
                        }
                        catch { }

                        try
                        {
                            myWordDoc.Close(false, missing, missing);
                            wordApp.Quit(false, missing, missing);
                        }
                        catch { }
                        throw new InvalidBestofQuiz("The \"best of 3 quizzes\" option is selected in the application settings, " +
                            "but the 3 quizzes are not selected in excel. \nPlease select the 3 quizzes in the excel file through \"Open Excel\" or uncheck this option from the settings panel.");
                    }
                }

                string fileNameOnly1 = SaveAs1.ToString().TrimEnd('.', 'd', 'o', 'c', 'x');
                int count1 = 1;
                while ((File.Exists(Path.Combine(SaveAs1.ToString()))))
                {
                    string tempFileName1 = string.Format("{0}({1})", fileNameOnly1, count1++);
                    SaveAs1 = tempFileName1 + ".docx";
                }

                progressBar2.Maximum = 0;
                for (int i = 31; i >= 2; i--)
                {
                    if (xlWorkSheet.Cells[2, i].Value2.ToString() == "True")
                        progressBar2.Maximum++;
                }
                int QuizMinimumIndex = 0;
                if (checkBox6.Checked) { 
                    if (double.Parse(xlWorkSheet.Cells[7, 3].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[7, 4].Value2.ToString()))
                    {
                        if (double.Parse(xlWorkSheet.Cells[7, 4].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[7, 5].Value2.ToString()))
                            QuizMinimumIndex = 3;
                        else if (double.Parse(xlWorkSheet.Cells[7, 3].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[7, 5].Value2.ToString()))
                            QuizMinimumIndex = 3;
                        else
                            QuizMinimumIndex = 5;
                    }
                    else
                    {
                        if (double.Parse(xlWorkSheet.Cells[7, 3].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[7, 5].Value2.ToString()))
                            QuizMinimumIndex = 4;
                        else if (double.Parse(xlWorkSheet.Cells[7, 4].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[7, 5].Value2.ToString()))
                            QuizMinimumIndex = 4;
                        else
                            QuizMinimumIndex = 5;
                    }
                }

                progressBar2.Visible = true;
                for (int i = 31; i >= 2; i--)
                {
                    if (xlWorkSheet.Cells[2, i].Value2.ToString() == "False")
                        continue;
                    if (checkBox6.Checked && i == QuizMinimumIndex)
                        continue;

                    Word.Application tempApp = new Word.Application();
                    Word.Document mytempDoc = null;

                    String filename;
                    if (int.Parse(xlWorkSheet.Cells[5, i].Value2.ToString()) == 1)
                        filename = form1Path;
                    else
                        filename = form2Path;

                    if (File.Exists((string)filename))
                    {
                        object readOnly = false;
                        tempApp.Visible = false;

                        try
                        {
                            mytempDoc = tempApp.Documents.Open(filename, ref missing, ref readOnly,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing, ref missing);
                            mytempDoc.Activate();
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                            xlWorkBook.Close(false, missing, missing);
                            myWordDoc.Close(false, missing, missing);
                            wordApp.Quit(false, missing, missing);
                            mytempDoc.Close(false, missing, missing);
                            tempApp.Quit(false, missing, missing);
                            xlApp.Quit();
                            return;
                        }


                        // calculate mark percentage
                        String StringASUMarkPercentage;
                        double ASUMarkPercentage;
                        double ASUMark;
                        double ASUMaxMark;
                        if (xlWorkSheet.Cells[6, i].Value2 != null && xlWorkSheet.Cells[7, i].Value2 != null)
                        {
                            try
                            {
                                ASUMark = double.Parse(xlWorkSheet.Cells[7, i].Value2.ToString());
                                ASUMaxMark = double.Parse(xlWorkSheet.Cells[6, i].Value2.ToString());
                            }
                            catch (Exception error)
                            {
                                MessageBox.Show(error.Message + "\n\n Try again after fixing the error in ASU mark.");
                                xlWorkBook.Close(false, missing, missing);
                                mytempDoc.Close(false, missing, missing);
                                myWordDoc.Close(false, missing, missing);
                                wordApp.Quit(false, missing, missing);
                                tempApp.Quit(false, missing, missing);
                                xlApp.Quit();
                                return;
                            }
                            if (ASUMaxMark <= 0)
                            {
                                MessageBox.Show("Max Mark can't be 0 or negative");
                                xlWorkBook.Close(false, missing, missing);
                                mytempDoc.Close(false, missing, missing);
                                tempApp.Quit(false, missing, missing);
                                myWordDoc.Close(false, missing, missing);
                                wordApp.Quit(false, missing, missing);
                                xlApp.Quit();
                                return;
                            }

                            ASUMarkPercentage = (ASUMark / ASUMaxMark) * 100;
                            ASUMarkPercentage = Math.Round(ASUMarkPercentage, 2);

                            if (checkBox5.Checked)
                                StringASUMarkPercentage = ASUMarkPercentage.ToString() + "%";
                            else
                                StringASUMarkPercentage = ASUMarkPercentage.ToString();
                        }
                        else if (xlWorkSheet.Cells[7, i].Value2 != null && xlWorkSheet.Cells[6, i].Value2 == null)
                        {
                            MessageBox.Show("Max Mark must have value");
                            xlWorkBook.Close(false, missing, missing);
                            mytempDoc.Close(false, missing, missing);
                            myWordDoc.Close(false, missing, missing);
                            wordApp.Quit(false, missing, missing);
                            tempApp.Quit(false, missing, missing);
                            xlApp.Quit();
                            return;
                        }
                        else
                        {
                            ASUMarkPercentage = 0;
                            StringASUMarkPercentage = "";
                        }
                        String StringUELMarkPercentage;
                        double UELMarkPercentage = 0;

                        //find and replace
                        this.FindAndReplace(tempApp, "<StudentName>", textBox10.Text);
                        this.FindAndReplace(tempApp, "<StudentID>", textBox11.Text);
                        this.FindAndReplace(tempApp, "<CCode>", comboBox15.Text);
                        this.FindAndReplace(tempApp, "<CName>", comboBox14.Text);
                        this.FindAndReplace(tempApp, "<Criteria>", xlWorkSheet.Cells[1, i].Value2.ToString());
                        this.FindAndReplace(tempApp, "<Mark>", StringASUMarkPercentage);
                        if (checkBox10.Checked)
                        {
                            if (comboBox9.SelectedIndex == 0)
                            {
                                this.FindAndReplaceImage(tempApp, "<FirstSignature>", textBox24.Text);
                                this.FindAndReplace(tempApp, "<Signature1>", "");
                            }
                            if (comboBox9.SelectedIndex == 1)
                            {
                                this.FindAndDeleteImage(tempApp, "<FirstSignature>");
                                this.FindAndReplace(tempApp, "<Signature1>", textBox18.Text);
                            }
                        }
                        else
                        {
                            this.FindAndDeleteImage(tempApp, "<FirstSignature>");
                            this.FindAndReplace(tempApp, "<Signature1>", "");
                        }
                        if (checkBox9.Checked)
                        {
                            if (comboBox8.SelectedIndex == 0)
                            {
                                this.FindAndReplaceImage(tempApp, "<SecondSignature>", textBox23.Text);
                                this.FindAndReplace(tempApp, "<Signature2>", "");
                            }
                            if (comboBox8.SelectedIndex == 1)
                            {
                                this.FindAndDeleteImage(tempApp, "<SecondSignature>");
                                this.FindAndReplace(tempApp, "<Signature2>", textBox17.Text);
                            }
                        }
                        else
                        {
                            this.FindAndDeleteImage(tempApp, "<SecondSignature>");
                            this.FindAndReplace(tempApp, "<Signature2>", "");
                        }
                        if (xlWorkSheet.Cells[3, i].Value2.ToString() == "False")
                        {
                            this.FindAndReplace(tempApp, "<Date>", "");
                        }
                        else
                        {
                            if (xlWorkSheet.Cells[4, i].Value2 == null)
                            {
                                MessageBox.Show("Date can't be empty" + "\n\nTry again after fixing the error.");
                                xlWorkBook.Close(false, missing, missing);
                                mytempDoc.Close(false, missing, missing);
                                myWordDoc.Close(false, missing, missing);
                                wordApp.Quit(false, missing, missing);
                                tempApp.Quit(false, missing, missing);
                                xlApp.Quit();
                                return;
                            }
                            else
                            {
                                string sDate = xlWorkSheet.Cells[4, i].Value2.ToString();
                                double date = double.Parse(sDate);
                                var dateTime = DateTime.FromOADate(date).ToString("dd/MM/yyyy");
                                this.FindAndReplace(tempApp, "<Date>", dateTime);
                            }
                        }


                        if (int.Parse(xlWorkSheet.Cells[5, i].Value2.ToString()) == 1)
                        {
                            switch (ASUMarkPercentage)
                            {
                                case double expression when expression >= 97:
                                    this.FindAndReplace(tempApp, "<c1>", "✔");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 100)
                                    {
                                        this.FindAndReplace(tempApp, "<b1>", "✔");
                                        this.FindAndReplace(tempApp, "<b2>", "");

                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b1>", "");
                                        this.FindAndReplace(tempApp, "<b2>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (200.0 / 3);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 93):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "✔");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");
                                    if (expression >= 96)
                                    {
                                        this.FindAndReplace(tempApp, "<b2>", "✔");
                                        this.FindAndReplace(tempApp, "<b3>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b2>", "");
                                        this.FindAndReplace(tempApp, "<b3>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (3.25) * ASUMarkPercentage - (220.25);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 89):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "✔");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");
                                    if (expression >= 90)
                                    {
                                        this.FindAndReplace(tempApp, "<b4>", "✔");
                                        this.FindAndReplace(tempApp, "<b5>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b4>", "");
                                        this.FindAndReplace(tempApp, "<b5>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 3.0 * ASUMarkPercentage - 197;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                    break;
                                case double expression when (expression >= 84):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "✔");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "✔");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");


                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 0.8 * ASUMarkPercentage - 1.2;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                    break;
                                case double expression when (expression >= 80):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "✔");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "✔");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 76):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "✔");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");
                                    if (expression >= 79)
                                    {
                                        this.FindAndReplace(tempApp, "<b7>", "✔");
                                        this.FindAndReplace(tempApp, "<b8>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b7>", "");
                                        this.FindAndReplace(tempApp, "<b8>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 73):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "✔");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");
                                    if (expression >= 75)
                                    {
                                        this.FindAndReplace(tempApp, "<b8>", "✔");
                                        this.FindAndReplace(tempApp, "<b9>", "");
                                        this.FindAndReplace(tempApp, "<b10>", "");
                                    }
                                    else if (expression >= 74)
                                    {
                                        this.FindAndReplace(tempApp, "<b8>", "");
                                        this.FindAndReplace(tempApp, "<b9>", "✔");
                                        this.FindAndReplace(tempApp, "<b10>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b8>", "");
                                        this.FindAndReplace(tempApp, "<b9>", "");
                                        this.FindAndReplace(tempApp, "<b10>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (4.0 / 3) * ASUMarkPercentage - (124.0 / 3);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 70):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "✔");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "✔");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = ASUMarkPercentage - 17.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 67):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "✔");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");
                                    if (expression >= 69)
                                    {
                                        this.FindAndReplace(tempApp, "<b10>", "✔");
                                        this.FindAndReplace(tempApp, "<b11>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b10>", "");
                                        this.FindAndReplace(tempApp, "<b11>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = ASUMarkPercentage - 17.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 64):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "✔");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "✔");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (185.0 / 3);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 60):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "✔");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "✔");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 1.25 * ASUMarkPercentage - 35;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression < 60):

                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");

                                    StringUELMarkPercentage = " ";

                                    if (xlWorkSheet.Cells[6, i].Value2 != null && xlWorkSheet.Cells[7, i].Value2 != null)
                                    {
                                        this.FindAndReplace(tempApp, "<c12>", "✔");
                                        if (expression >= 59)
                                        {
                                            this.FindAndReplace(tempApp, "<b13>", "✔");
                                            this.FindAndReplace(tempApp, "<b14>", "");
                                            this.FindAndReplace(tempApp, "<b15>", "");
                                            this.FindAndReplace(tempApp, "<b16>", "");
                                        }
                                        else if (expression >= 40)
                                        {
                                            this.FindAndReplace(tempApp, "<b13>", "");
                                            this.FindAndReplace(tempApp, "<b14>", "✔");
                                            this.FindAndReplace(tempApp, "<b15>", "");
                                            this.FindAndReplace(tempApp, "<b16>", "");
                                        }
                                        else if (expression >= 20)
                                        {
                                            this.FindAndReplace(tempApp, "<b13>", "");
                                            this.FindAndReplace(tempApp, "<b14>", "");
                                            this.FindAndReplace(tempApp, "<b15>", "✔");
                                            this.FindAndReplace(tempApp, "<b16>", "");
                                        }
                                        else
                                        {
                                            this.FindAndReplace(tempApp, "<b13>", "");
                                            this.FindAndReplace(tempApp, "<b14>", "");
                                            this.FindAndReplace(tempApp, "<b15>", "");
                                            this.FindAndReplace(tempApp, "<b16>", "✔");
                                        }

                                        if (checkBox4.Checked == true)
                                        {
                                            UELMarkPercentage = (2.0 / 3) * ASUMarkPercentage;
                                            UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                            if (checkBox5.Checked)
                                                StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                            else
                                                StringUELMarkPercentage = UELMarkPercentage.ToString();
                                        }
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<c12>", "");
                                        this.FindAndReplace(tempApp, "<b13>", "");
                                        this.FindAndReplace(tempApp, "<b14>", "");
                                        this.FindAndReplace(tempApp, "<b15>", "");
                                        this.FindAndReplace(tempApp, "<b16>", "");
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);


                                    break;
                                default:
                                    StringUELMarkPercentage = " ";
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                    MessageBox.Show("ERROR 404");
                                    xlWorkBook.Close(false, missing, missing);
                                    mytempDoc.Close(false, missing, missing);
                                    myWordDoc.Close(false, missing, missing);
                                    wordApp.Quit(false, missing, missing);
                                    tempApp.Quit(false, missing, missing);
                                    xlApp.Quit();
                                    return;
                            }
                        }
                        else
                        {
                            switch (ASUMarkPercentage)
                            {
                                case double expression when expression >= 97:
                                    this.FindAndReplace(tempApp, "<c1>", "✔");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 100)
                                    {
                                        this.FindAndReplace(tempApp, "<b1>", "✔");
                                        this.FindAndReplace(tempApp, "<b2>", "");

                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b1>", "");
                                        this.FindAndReplace(tempApp, "<b2>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (200.0 / 3);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 93):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "✔");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");
                                    if (expression >= 96)
                                    {
                                        this.FindAndReplace(tempApp, "<b2>", "✔");
                                        this.FindAndReplace(tempApp, "<b3>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b2>", "");
                                        this.FindAndReplace(tempApp, "<b3>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (3.25) * ASUMarkPercentage - (220.25);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 89):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "✔");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");
                                    if (expression >= 92)
                                    {
                                        this.FindAndReplace(tempApp, "<b3>", "✔");
                                        this.FindAndReplace(tempApp, "<b4>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b3>", "");
                                        this.FindAndReplace(tempApp, "<b4>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 3.0 * ASUMarkPercentage - 197;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                    break;
                                case double expression when (expression >= 84):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "✔");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 88)
                                    {
                                        this.FindAndReplace(tempApp, "<b5>", "✔");
                                        this.FindAndReplace(tempApp, "<b6>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b5>", "");
                                        this.FindAndReplace(tempApp, "<b6>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 0.8 * ASUMarkPercentage - 1.2;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                    break;
                                case double expression when (expression >= 80):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "✔");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "✔");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 76):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "✔");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "✔");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 73):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "✔");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 75)
                                    {
                                        this.FindAndReplace(tempApp, "<b9>", "✔");
                                        this.FindAndReplace(tempApp, "<b10>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b9>", "");
                                        this.FindAndReplace(tempApp, "<b10>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (4.0 / 3) * ASUMarkPercentage - (124.0 / 3);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 70):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "✔");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 72)
                                    {
                                        this.FindAndReplace(tempApp, "<b10>", "✔");
                                        this.FindAndReplace(tempApp, "<b11>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b10>", "");
                                        this.FindAndReplace(tempApp, "<b11>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = ASUMarkPercentage - 17.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 67):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "✔");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 69)
                                    {
                                        this.FindAndReplace(tempApp, "<b11>", "✔");
                                        this.FindAndReplace(tempApp, "<b12>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b11>", "");
                                        this.FindAndReplace(tempApp, "<b12>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = ASUMarkPercentage - 17.0;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 64):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "✔");
                                    this.FindAndReplace(tempApp, "<c11>", "");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 66)
                                    {
                                        this.FindAndReplace(tempApp, "<b13>", "✔");
                                        this.FindAndReplace(tempApp, "<b14>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b13>", "");
                                        this.FindAndReplace(tempApp, "<b14>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox4.Checked == true)
                                    {
                                        UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (185.0 / 3);
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression >= 60):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "✔");
                                    this.FindAndReplace(tempApp, "<c12>", "");

                                    if (expression >= 62)
                                    {
                                        this.FindAndReplace(tempApp, "<b15>", "✔");
                                        this.FindAndReplace(tempApp, "<b16>", "");
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<b15>", "");
                                        this.FindAndReplace(tempApp, "<b16>", "✔");
                                    }
                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b17>", "");
                                    this.FindAndReplace(tempApp, "<b18>", "");
                                    this.FindAndReplace(tempApp, "<b19>", "");
                                    this.FindAndReplace(tempApp, "<b20>", "");

                                    if (checkBox1.Checked == true)
                                    {
                                        UELMarkPercentage = 1.25 * ASUMarkPercentage - 35;
                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                        if (checkBox5.Checked)
                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                        else
                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                    }
                                    else
                                    {
                                        StringUELMarkPercentage = " ";
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                case double expression when (expression < 60):
                                    this.FindAndReplace(tempApp, "<c1>", "");
                                    this.FindAndReplace(tempApp, "<c2>", "");
                                    this.FindAndReplace(tempApp, "<c3>", "");
                                    this.FindAndReplace(tempApp, "<c4>", "");
                                    this.FindAndReplace(tempApp, "<c5>", "");
                                    this.FindAndReplace(tempApp, "<c6>", "");
                                    this.FindAndReplace(tempApp, "<c7>", "");
                                    this.FindAndReplace(tempApp, "<c8>", "");
                                    this.FindAndReplace(tempApp, "<c9>", "");
                                    this.FindAndReplace(tempApp, "<c10>", "");
                                    this.FindAndReplace(tempApp, "<c11>", "");

                                    this.FindAndReplace(tempApp, "<b1>", "");
                                    this.FindAndReplace(tempApp, "<b2>", "");
                                    this.FindAndReplace(tempApp, "<b3>", "");
                                    this.FindAndReplace(tempApp, "<b4>", "");
                                    this.FindAndReplace(tempApp, "<b5>", "");
                                    this.FindAndReplace(tempApp, "<b6>", "");
                                    this.FindAndReplace(tempApp, "<b7>", "");
                                    this.FindAndReplace(tempApp, "<b8>", "");
                                    this.FindAndReplace(tempApp, "<b9>", "");
                                    this.FindAndReplace(tempApp, "<b10>", "");
                                    this.FindAndReplace(tempApp, "<b11>", "");
                                    this.FindAndReplace(tempApp, "<b12>", "");
                                    this.FindAndReplace(tempApp, "<b13>", "");
                                    this.FindAndReplace(tempApp, "<b14>", "");
                                    this.FindAndReplace(tempApp, "<b15>", "");
                                    this.FindAndReplace(tempApp, "<b16>", "");

                                    StringUELMarkPercentage = " ";

                                    if (xlWorkSheet.Cells[6, i].Value2 != null && xlWorkSheet.Cells[7, i].Value2 != null)
                                    {
                                        this.FindAndReplace(tempApp, "<c12>", "✔");
                                        if (expression >= 59)
                                        {
                                            this.FindAndReplace(tempApp, "<b17>", "✔");
                                            this.FindAndReplace(tempApp, "<b18>", "");
                                            this.FindAndReplace(tempApp, "<b19>", "");
                                            this.FindAndReplace(tempApp, "<b20>", "");
                                        }
                                        else if (expression >= 40)
                                        {
                                            this.FindAndReplace(tempApp, "<b17>", "");
                                            this.FindAndReplace(tempApp, "<b18>", "✔");
                                            this.FindAndReplace(tempApp, "<b19>", "");
                                            this.FindAndReplace(tempApp, "<b20>", "");
                                        }
                                        else if (expression >= 20)
                                        {
                                            this.FindAndReplace(tempApp, "<b17>", "");
                                            this.FindAndReplace(tempApp, "<b18>", "");
                                            this.FindAndReplace(tempApp, "<b19>", "✔");
                                            this.FindAndReplace(tempApp, "<b20>", "");
                                        }
                                        else
                                        {
                                            this.FindAndReplace(tempApp, "<b17>", "");
                                            this.FindAndReplace(tempApp, "<b18>", "");
                                            this.FindAndReplace(tempApp, "<b19>", "");
                                            this.FindAndReplace(tempApp, "<b20>", "✔");
                                        }
                                        if (checkBox4.Checked == true)
                                        {
                                            UELMarkPercentage = (2.0 / 3) * ASUMarkPercentage;
                                            UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                            if (checkBox5.Checked)
                                                StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                            else
                                                StringUELMarkPercentage = UELMarkPercentage.ToString();
                                        }
                                    }
                                    else
                                    {
                                        this.FindAndReplace(tempApp, "<c12>", "");
                                        this.FindAndReplace(tempApp, "<b17>", "");
                                        this.FindAndReplace(tempApp, "<b18>", "");
                                        this.FindAndReplace(tempApp, "<b19>", "");
                                        this.FindAndReplace(tempApp, "<b20>", "");
                                    }
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                    break;
                                default:
                                    StringUELMarkPercentage = " ";
                                    this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                    MessageBox.Show("ERROR 404");
                                    xlWorkBook.Close(false, missing, missing);
                                    mytempDoc.Close(false, missing, missing);
                                    tempApp.Quit(false, missing, missing);
                                    myWordDoc.Close(false, missing, missing);
                                    wordApp.Quit(false, missing, missing);
                                    xlApp.Quit();
                                    return;
                            }
                        }

                    }
                    else
                    {
                        MessageBox.Show("File not Found!");
                        xlWorkBook.Close(false, missing, missing);
                        myWordDoc.Close(false, missing, missing);
                        wordApp.Quit(false, missing, missing);
                        mytempDoc.Close(false, missing, missing);
                        tempApp.Quit(false, missing, missing);
                        xlApp.Quit();
                        return;
                    }
                    int tempcount = 1;
                    string fileNameOnlytemp = "temp";
                    string path = textBox8.Text + @"\" + fileNameOnlytemp + ".docx";
                    while ((File.Exists(Path.Combine(textBox8.Text + fileNameOnlytemp))))
                    {
                        string tempFileName1 = string.Format("{0}({1})", fileNameOnlytemp, tempcount++);
                        path = textBox8.Text + @"\" + tempFileName1 + ".docx";
                    }
                    object oPageBreak = WdBreakType.wdPageBreak;
                    if (isFirstTime)
                    {
                        mytempDoc.SaveAs2(SaveAs1, ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);
                        isFirstTime = false;
                        try
                        {
                            object readOnly = false;
                            wordApp.Visible = false;
                            mytempDoc.Close();
                            tempApp.Quit();
                            myWordDoc = wordApp.Documents.Open(SaveAs1, ref missing, ref readOnly,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing, ref missing);
                            myWordDoc.Activate();
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                            xlWorkBook.Close(false, missing, missing);
                            myWordDoc?.Close(false, missing, missing);
                            wordApp.Quit(false, missing, missing);
                            mytempDoc.Close(false, missing, missing);
                            tempApp.Quit(false, missing, missing);
                            xlApp.Quit();
                            return;
                        }
                    }
                    else
                    {
                        mytempDoc.SaveAs2(path, ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);
                        mytempDoc.Close();
                        tempApp.Quit();
                        wordApp.Selection.Range.InsertBreak(ref oPageBreak);
                        wordApp.Selection.Range.InsertFile(path, ref missing, ref missing, ref missing, ref missing);
                        myWordDoc.SaveAs2(ref SaveAs1, ref missing, ref missing, ref missing,
                                       ref missing, ref missing, ref missing,
                                       ref missing, ref missing, ref missing,
                                       ref missing, ref missing, ref missing,
                                       ref missing, ref missing, ref missing);

                    }
                    if (File.Exists(Path.Combine(path)))
                    {
                        File.Delete(Path.Combine(path));
                    }
                    progressBar2.Value++;
                }
                //Save as
                try
                {
                    string fileNameOnly2 = SaveAs2.ToString().TrimEnd('.', 'p', 'd', 'f');
                    int count2 = 1;
                    while ((File.Exists(Path.Combine(SaveAs2.ToString()))))
                    {
                        string tempFileName2 = string.Format("{0}({1})", fileNameOnly2, count2++);
                        SaveAs2 = tempFileName2 + ".pdf";
                    }
                    if (SaveAsType == 1)
                    {
                        MessageBox.Show("File Created!");
                        myWordDoc.Close();
                        xlWorkBook.Close();
                        xlApp.Quit();
                        wordApp.Quit();
                    }
                    else if (SaveAsType == 2)
                    {
                        myWordDoc.SaveAs2(ref SaveAs2, Word.WdSaveFormat.wdFormatPDF, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);
                        myWordDoc.Close();
                        xlWorkBook.Close();
                        xlApp.Quit();
                        wordApp.Quit();

                        if (File.Exists(Path.Combine(SaveAs1.ToString())))
                        {
                            File.Delete(Path.Combine(SaveAs1.ToString()));
                        }
                        MessageBox.Show("File Created!");
                    }
                    else if (SaveAsType == 3)
                    {
                        myWordDoc.SaveAs2(ref SaveAs2, Word.WdSaveFormat.wdFormatPDF, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing,
                                   ref missing, ref missing, ref missing);
                        MessageBox.Show("File Created!");
                        myWordDoc.Close();
                        xlWorkBook.Close();
                        xlApp.Quit();
                        wordApp.Quit();
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                    xlWorkBook.Close(false, missing, missing);
                    myWordDoc.Close(false, missing, missing);
                    wordApp.Quit(false, missing, missing);
                    xlApp.Quit();
                    return;
                }
                progressBar2.Visible = false;
            }
            catch (Exception e)
            {
                progressBar2.Visible = false;

                try
                {
                    try
                    {
                        myWordDoc.Close(false, missing, missing);
                        wordApp.Quit(false, missing, missing);
                    }
                    catch
                    {
                        try
                        {
                            wordApp.Quit(false, missing, missing);
                        }
                        catch { }
                    }

                    try
                    {
                        xlWorkBook.Close(false, missing, missing);
                        xlApp.Quit();
                    }
                    catch { }
                }
                catch { }
                finally
                {

                    if (e is InvalidBestofQuiz)
                        throw e;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (checkBox9.Checked && comboBox8.SelectedIndex == 0 && textBox23.Text == "" || checkBox10.Checked && comboBox9.SelectedIndex == 0 && textBox24.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }

            String filename = textBox11.Text + " - " + "Feedbacks";
            try
            {
                CreateWordDocumentExcel(textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 1);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
            }

        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (checkBox9.Checked && comboBox8.SelectedIndex == 0 && textBox23.Text == "" || checkBox10.Checked && comboBox9.SelectedIndex == 0 && textBox24.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            String filename = textBox11.Text + " - " + "Feedbacks";
            try
            {
                CreateWordDocumentExcel(textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 2);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (checkBox9.Checked && comboBox8.SelectedIndex == 0 && textBox23.Text == "" || checkBox10.Checked && comboBox9.SelectedIndex == 0 && textBox24.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            String filename = textBox11.Text + " - " + "Feedbacks";
            try
            {
                CreateWordDocumentExcel(textBox8.Text + @"\" + filename + ".docx", textBox8.Text + @"\" + filename + ".pdf", 3);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            {
                String filename = InstructorPath;
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkBook = null;
                Excel.Worksheet xlWorkSheet;
                object missing = System.Reflection.Missing.Value;

                if (!File.Exists((string)filename))
                {
                    InstructorPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Instructor.xlsx");
                    ExtractEmbeddedFile("AutoFeedbacks.Instructor.xlsx", InstructorPath);
                    filename = textBox28.Text = InstructorPath;
                }


                if (File.Exists((string)filename))
                {
                    object readOnly = false;
                    xlApp.Visible = true;

                    try
                    {
                        xlWorkBook = xlApp.Workbooks.Open(filename, missing, readOnly,
                                                missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing);
                        xlWorkBook.Activate();
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                        xlWorkBook.Close(false, missing, missing);
                        xlApp.Quit();
                        return;
                    }
                }
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
            }
        }
        private void InstructorCreateWordDocumentExcel(object filePath, int SaveAsType)
        {
            object missing = Missing.Value;
            String filename2 = InstructorPath;
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkBook = null;
            Excel.Worksheet xlWorkSheet;

            try
            {
                if (!File.Exists((string)filename2))
                {
                    InstructorPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Instructor.xlsx");
                    ExtractEmbeddedFile("AutoFeedbacks.Instructor.xlsx", InstructorPath);
                    filename2 = textBox28.Text = InstructorPath;
                }
                if (File.Exists((string)filename2))
                {
                    object readOnly2 = false;
                    xlApp.Visible = false;
                    try
                    {
                        xlWorkBook = xlApp.Workbooks.Open(filename2, missing, readOnly2,
                                                missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing,
                                                missing, missing, missing);
                        xlWorkBook.Activate();
                    }
                    catch (Exception error)
                    {
                        MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                        xlWorkBook.Close(false, missing, missing);
                        xlApp.Quit();
                        return;
                    }
                }
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                if (checkBox6.Checked)
                {
                    if (!(xlWorkSheet.Cells[2, 4].Value2.ToString() == "True" && xlWorkSheet.Cells[2, 5].Value2.ToString() == "True" && xlWorkSheet.Cells[2, 6].Value2.ToString() == "True"))
                    {
                        xlWorkBook.Close(false, missing, missing);
                        xlApp.Quit();
                        throw new InvalidBestofQuiz("The \"best of 3 quizzes\" option is selected in the application settings, " +
                            "but the 3 quizzes are not selected in excel. \nPlease select the 3 quizzes in the excel file through \"Open Excel\" or uncheck this option from the settings panel.");
                    }
                }

                int tempcount = 1;
                string fileNameOnlytemp = "temp";
                string path = filePath + @"\" + fileNameOnlytemp + ".docx";
                while ((File.Exists(Path.Combine(filePath + fileNameOnlytemp))))
                {
                    string tempFileName1 = string.Format("{0}({1})", fileNameOnlytemp, tempcount++);
                    path = filePath + @"\" + tempFileName1 + ".docx";
                }
                int rowCounter = 9;
                int operationCounter = 0;
                progressBar1.Maximum = 0;

                // count number of operations (Assignment ,Quizzes , Midterm ...etc)
                for (int i = 3; i <= 31; i++)
                {
                    if (xlWorkSheet.Cells[2, i].Value2.ToString() == "False" && i>12)
                        break;

                    if (xlWorkSheet.Cells[2, i].Value2.ToString() == "False")
                        continue;

                    operationCounter++;
                 }
                if (checkBox6.Checked)
                {
                    operationCounter--;
                }
                progressBar1.Maximum = operationCounter;
                operationCounter = 0;

                // count number of Students
                while ((xlWorkSheet.Cells[rowCounter + operationCounter, 1].Value2 != null) || (xlWorkSheet.Cells[rowCounter + operationCounter, 2].Value2 != null))
                    {
                    operationCounter++;
                    }
                progressBar1.Maximum *= operationCounter;
                progressBar1.Visible = true;
                do
                {
                    object SaveAs1 = filePath + @"\" + xlWorkSheet.Cells[rowCounter, 2].Value2.ToString() + " - Feedbacks" + ".docx";
                    object SaveAs2 = filePath + @"\" + xlWorkSheet.Cells[rowCounter, 2].Value2.ToString() + " - Feedbacks" + ".pdf";
                    Word.Application wordApp = new Word.Application();
                    Word.Document myWordDoc = null;
                    bool isFirstTime = true;
                    try
                    {

                        string fileNameOnly1 = SaveAs1.ToString().TrimEnd('.', 'd', 'o', 'c', 'x');
                        int count1 = 1;
                        while ((File.Exists(Path.Combine(SaveAs1.ToString()))))
                        {
                            string tempFileName1 = string.Format("{0}({1})", fileNameOnly1, count1++);
                            SaveAs1 = tempFileName1 + ".docx";
                        }

                        string fileNameOnly2 = SaveAs2.ToString().TrimEnd('.', 'p', 'd', 'f');
                        int count2 = 1;
                        while ((File.Exists(Path.Combine(SaveAs2.ToString()))))
                        {
                            string tempFileName2 = string.Format("{0}({1})", fileNameOnly2, count2++);
                            SaveAs2 = tempFileName2 + ".pdf";
                        }
                        int QuizMinimumIndex = 0;
                        if (checkBox6.Checked)
                        {
                            if (double.Parse(xlWorkSheet.Cells[rowCounter, 4].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[rowCounter, 5].Value2.ToString()))
                            {
                                if (double.Parse(xlWorkSheet.Cells[rowCounter, 5].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[rowCounter, 6].Value2.ToString()))
                                    QuizMinimumIndex = 4;
                                else if (double.Parse(xlWorkSheet.Cells[rowCounter, 4].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[rowCounter, 5].Value2.ToString()))
                                    QuizMinimumIndex = 4;
                                else
                                    QuizMinimumIndex = 6;
                            }
                            else
                            {
                                if (double.Parse(xlWorkSheet.Cells[rowCounter, 4].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[rowCounter, 6].Value2.ToString()))
                                    QuizMinimumIndex = 5;
                                else if (double.Parse(xlWorkSheet.Cells[rowCounter, 5].Value2.ToString()) < double.Parse(xlWorkSheet.Cells[rowCounter, 6].Value2.ToString()))
                                    QuizMinimumIndex = 5;
                                else
                                    QuizMinimumIndex = 6;
                            }
                        }
                        for (int i = 31; i >= 3; i--)
                        {
                            if (xlWorkSheet.Cells[2, i].Value2.ToString() == "False")
                                continue;
                            if (checkBox6.Checked && i == QuizMinimumIndex)
                                continue;

                            Word.Application tempApp = new Word.Application();
                            Word.Document mytempDoc = null;

                            try
                            {

                                String filename;
                                if (int.Parse(xlWorkSheet.Cells[5, i].Value2.ToString()) == 1)
                                    filename = form1Path;
                                else
                                    filename = form2Path;

                                if (File.Exists((string)filename))
                                {
                                    object readOnly = false;
                                    tempApp.Visible = false;

                                    try
                                    {
                                        mytempDoc = tempApp.Documents.Open(filename, ref missing, ref readOnly,
                                                           ref missing, ref missing, ref missing,
                                                           ref missing, ref missing, ref missing,
                                                           ref missing, ref missing, ref missing,
                                                           ref missing, ref missing, ref missing, ref missing);
                                        mytempDoc.Activate();
                                    }
                                    catch (Exception error)
                                    {
                                        MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                                        xlWorkBook.Close(false, missing, missing);
                                        myWordDoc.Close(false, missing, missing);
                                        wordApp.Quit(false, missing, missing);
                                        mytempDoc.Close(false, missing, missing);
                                        tempApp.Quit(false, missing, missing);
                                        xlApp.Quit();
                                        return;
                                    }

                                    // calculate mark percentage
                                    String StringASUMarkPercentage;
                                    double ASUMarkPercentage;
                                    double ASUMark;
                                    double ASUMaxMark;
                                    if (xlWorkSheet.Cells[6, i].Value2 != null && xlWorkSheet.Cells[rowCounter, i].Value2 != null)
                                    {
                                        try
                                        {
                                            ASUMark = double.Parse(xlWorkSheet.Cells[rowCounter, i].Value2.ToString());
                                            ASUMaxMark = double.Parse(xlWorkSheet.Cells[6, i].Value2.ToString());
                                        }
                                        catch (Exception error)
                                        {
                                            MessageBox.Show(error.Message + "\n\n Try again after fixing the error in ASU mark.");
                                            xlWorkBook.Close(false, missing, missing);
                                            mytempDoc.Close(false, missing, missing);
                                            myWordDoc.Close(false, missing, missing);
                                            wordApp.Quit(false, missing, missing);
                                            tempApp.Quit(false, missing, missing);
                                            xlApp.Quit();
                                            return;
                                        }
                                        if (ASUMaxMark <= 0)
                                        {
                                            MessageBox.Show("Max Mark can't be 0 or negative");
                                            xlWorkBook.Close(false, missing, missing);
                                            mytempDoc.Close(false, missing, missing);
                                            tempApp.Quit(false, missing, missing);
                                            myWordDoc.Close(false, missing, missing);
                                            wordApp.Quit(false, missing, missing);
                                            xlApp.Quit();
                                            return;
                                        }

                                        ASUMarkPercentage = (ASUMark / ASUMaxMark) * 100;
                                        ASUMarkPercentage = Math.Round(ASUMarkPercentage, 2);

                                        if (checkBox5.Checked)
                                            StringASUMarkPercentage = ASUMarkPercentage.ToString() + "%";
                                        else
                                            StringASUMarkPercentage = ASUMarkPercentage.ToString();
                                    }
                                    else if (xlWorkSheet.Cells[rowCounter, i].Value2 != null && xlWorkSheet.Cells[6, i].Value2 == null)
                                    {
                                        MessageBox.Show("Max Mark must have value");
                                        xlWorkBook.Close(false, missing, missing);
                                        mytempDoc.Close(false, missing, missing);
                                        myWordDoc.Close(false, missing, missing);
                                        wordApp.Quit(false, missing, missing);
                                        tempApp.Quit(false, missing, missing);
                                        xlApp.Quit();
                                        return;
                                    }
                                    else
                                    {
                                        ASUMarkPercentage = 0;
                                        StringASUMarkPercentage = "";
                                    }
                                    String StringUELMarkPercentage;
                                    double UELMarkPercentage = 0;

                                    //find and replace
                                    this.FindAndReplace(tempApp, "<StudentName>", xlWorkSheet.Cells[rowCounter, 1].Value2.ToString());
                                    this.FindAndReplace(tempApp, "<StudentID>", xlWorkSheet.Cells[rowCounter, 2].Value2.ToString());
                                    this.FindAndReplace(tempApp, "<CCode>", comboBox17.Text);
                                    this.FindAndReplace(tempApp, "<CName>", comboBox16.Text);
                                    this.FindAndReplace(tempApp, "<Criteria>", xlWorkSheet.Cells[1, i].Value2.ToString());
                                    this.FindAndReplace(tempApp, "<Mark>", StringASUMarkPercentage);
                                    if (checkBox12.Checked)
                                    {
                                        if (comboBox11.SelectedIndex == 0)
                                        {
                                            this.FindAndReplaceImage(tempApp, "<FirstSignature>", textBox26.Text);
                                            this.FindAndReplace(tempApp, "<Signature1>", "");
                                        }
                                        if (comboBox11.SelectedIndex == 1)
                                        {
                                            this.FindAndDeleteImage(tempApp, "<FirstSignature>");
                                            this.FindAndReplace(tempApp, "<Signature1>", textBox20.Text);
                                        }
                                    }
                                    else
                                    {
                                        this.FindAndDeleteImage(tempApp, "<FirstSignature>");
                                        this.FindAndReplace(tempApp, "<Signature1>", "");
                                    }
                                    if (checkBox11.Checked)
                                    {
                                        if (comboBox10.SelectedIndex == 0)
                                        {
                                            this.FindAndReplaceImage(tempApp, "<SecondSignature>", textBox25.Text);
                                            this.FindAndReplace(tempApp, "<Signature2>", "");
                                        }
                                        if (comboBox10.SelectedIndex == 1)
                                        {
                                            this.FindAndDeleteImage(tempApp, "<SecondSignature>");
                                            this.FindAndReplace(tempApp, "<Signature2>", textBox19.Text);
                                        }
                                    }
                                    else
                                    {
                                        this.FindAndDeleteImage(tempApp, "<SecondSignature>");
                                        this.FindAndReplace(tempApp, "<Signature2>", "");
                                    }
                                    if (xlWorkSheet.Cells[3, i].Value2.ToString() == "False")
                                    {
                                        this.FindAndReplace(tempApp, "<Date>", "");
                                    }
                                    else
                                    {
                                        if (xlWorkSheet.Cells[4, i].Value2 == null)
                                        {
                                            MessageBox.Show("Date can't be empty" + "\n\nTry again after fixing the error.");
                                            xlWorkBook.Close(false, missing, missing);
                                            mytempDoc.Close(false, missing, missing);
                                            myWordDoc.Close(false, missing, missing);
                                            wordApp.Quit(false, missing, missing);
                                            tempApp.Quit(false, missing, missing);
                                            xlApp.Quit();
                                            return;
                                        }
                                        else
                                        {
                                            string sDate = xlWorkSheet.Cells[4, i].Value2.ToString();
                                            double date = double.Parse(sDate);
                                            var dateTime = DateTime.FromOADate(date).ToString("dd/MM/yyyy");
                                            this.FindAndReplace(tempApp, "<Date>", dateTime);
                                        }
                                    }


                                    if (int.Parse(xlWorkSheet.Cells[5, i].Value2.ToString()) == 1)
                                    {
                                        switch (ASUMarkPercentage)
                                        {
                                            case double expression when expression >= 97:
                                                this.FindAndReplace(tempApp, "<c1>", "✔");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 100)
                                                {
                                                    this.FindAndReplace(tempApp, "<b1>", "✔");
                                                    this.FindAndReplace(tempApp, "<b2>", "");

                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b1>", "");
                                                    this.FindAndReplace(tempApp, "<b2>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (200.0 / 3);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 93):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "✔");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");
                                                if (expression >= 96)
                                                {
                                                    this.FindAndReplace(tempApp, "<b2>", "✔");
                                                    this.FindAndReplace(tempApp, "<b3>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b2>", "");
                                                    this.FindAndReplace(tempApp, "<b3>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (3.25) * ASUMarkPercentage - (220.25);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 89):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "✔");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");
                                                if (expression >= 90)
                                                {
                                                    this.FindAndReplace(tempApp, "<b4>", "✔");
                                                    this.FindAndReplace(tempApp, "<b5>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b4>", "");
                                                    this.FindAndReplace(tempApp, "<b5>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 3.0 * ASUMarkPercentage - 197;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                                break;
                                            case double expression when (expression >= 84):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "✔");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "✔");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");


                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 0.8 * ASUMarkPercentage - 1.2;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                                break;
                                            case double expression when (expression >= 80):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "✔");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "✔");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 76):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "✔");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");
                                                if (expression >= 79)
                                                {
                                                    this.FindAndReplace(tempApp, "<b7>", "✔");
                                                    this.FindAndReplace(tempApp, "<b8>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b7>", "");
                                                    this.FindAndReplace(tempApp, "<b8>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 73):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "✔");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");
                                                if (expression >= 75)
                                                {
                                                    this.FindAndReplace(tempApp, "<b8>", "✔");
                                                    this.FindAndReplace(tempApp, "<b9>", "");
                                                    this.FindAndReplace(tempApp, "<b10>", "");
                                                }
                                                else if (expression >= 74)
                                                {
                                                    this.FindAndReplace(tempApp, "<b8>", "");
                                                    this.FindAndReplace(tempApp, "<b9>", "✔");
                                                    this.FindAndReplace(tempApp, "<b10>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b8>", "");
                                                    this.FindAndReplace(tempApp, "<b9>", "");
                                                    this.FindAndReplace(tempApp, "<b10>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (4.0 / 3) * ASUMarkPercentage - (124.0 / 3);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 70):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "✔");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "✔");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 67):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "✔");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");
                                                if (expression >= 69)
                                                {
                                                    this.FindAndReplace(tempApp, "<b10>", "✔");
                                                    this.FindAndReplace(tempApp, "<b11>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b10>", "");
                                                    this.FindAndReplace(tempApp, "<b11>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 64):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "✔");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "✔");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (185.0 / 3);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 60):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "✔");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "✔");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 1.25 * ASUMarkPercentage - 35;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression < 60):

                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");

                                                StringUELMarkPercentage = " ";

                                                if (xlWorkSheet.Cells[6, i].Value2 != null && xlWorkSheet.Cells[rowCounter, i].Value2 != null)
                                                {
                                                    this.FindAndReplace(tempApp, "<c12>", "✔");
                                                    if (expression >= 59)
                                                    {
                                                        this.FindAndReplace(tempApp, "<b13>", "✔");
                                                        this.FindAndReplace(tempApp, "<b14>", "");
                                                        this.FindAndReplace(tempApp, "<b15>", "");
                                                        this.FindAndReplace(tempApp, "<b16>", "");
                                                    }
                                                    else if (expression >= 40)
                                                    {
                                                        this.FindAndReplace(tempApp, "<b13>", "");
                                                        this.FindAndReplace(tempApp, "<b14>", "✔");
                                                        this.FindAndReplace(tempApp, "<b15>", "");
                                                        this.FindAndReplace(tempApp, "<b16>", "");
                                                    }
                                                    else if (expression >= 20)
                                                    {
                                                        this.FindAndReplace(tempApp, "<b13>", "");
                                                        this.FindAndReplace(tempApp, "<b14>", "");
                                                        this.FindAndReplace(tempApp, "<b15>", "✔");
                                                        this.FindAndReplace(tempApp, "<b16>", "");
                                                    }
                                                    else
                                                    {
                                                        this.FindAndReplace(tempApp, "<b13>", "");
                                                        this.FindAndReplace(tempApp, "<b14>", "");
                                                        this.FindAndReplace(tempApp, "<b15>", "");
                                                        this.FindAndReplace(tempApp, "<b16>", "✔");
                                                    }

                                                    if (checkBox3.Checked == true)
                                                    {
                                                        UELMarkPercentage = (2.0 / 3) * ASUMarkPercentage;
                                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                        if (checkBox5.Checked)
                                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                        else
                                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<c12>", "");
                                                    this.FindAndReplace(tempApp, "<b13>", "");
                                                    this.FindAndReplace(tempApp, "<b14>", "");
                                                    this.FindAndReplace(tempApp, "<b15>", "");
                                                    this.FindAndReplace(tempApp, "<b16>", "");
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);


                                                break;
                                            default:
                                                StringUELMarkPercentage = " ";
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                                MessageBox.Show("ERROR 404");
                                                xlWorkBook.Close(false, missing, missing);
                                                mytempDoc.Close(false, missing, missing);
                                                myWordDoc.Close(false, missing, missing);
                                                wordApp.Quit(false, missing, missing);
                                                tempApp.Quit(false, missing, missing);
                                                xlApp.Quit();
                                                return;
                                        }
                                    }
                                    else
                                    {
                                        switch (ASUMarkPercentage)
                                        {
                                            case double expression when expression >= 97:
                                                this.FindAndReplace(tempApp, "<c1>", "✔");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 100)
                                                {
                                                    this.FindAndReplace(tempApp, "<b1>", "✔");
                                                    this.FindAndReplace(tempApp, "<b2>", "");

                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b1>", "");
                                                    this.FindAndReplace(tempApp, "<b2>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (200.0 / 3);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 93):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "✔");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");
                                                if (expression >= 96)
                                                {
                                                    this.FindAndReplace(tempApp, "<b2>", "✔");
                                                    this.FindAndReplace(tempApp, "<b3>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b2>", "");
                                                    this.FindAndReplace(tempApp, "<b3>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (3.25) * ASUMarkPercentage - (220.25);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 89):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "✔");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");
                                                if (expression >= 92)
                                                {
                                                    this.FindAndReplace(tempApp, "<b3>", "✔");
                                                    this.FindAndReplace(tempApp, "<b4>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b3>", "");
                                                    this.FindAndReplace(tempApp, "<b4>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 3.0 * ASUMarkPercentage - 197;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                                break;
                                            case double expression when (expression >= 84):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "✔");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 88)
                                                {
                                                    this.FindAndReplace(tempApp, "<b5>", "✔");
                                                    this.FindAndReplace(tempApp, "<b6>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b5>", "");
                                                    this.FindAndReplace(tempApp, "<b6>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 0.8 * ASUMarkPercentage - 1.2;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                                break;
                                            case double expression when (expression >= 80):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "✔");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "✔");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 76):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "✔");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "✔");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = 0.75 * ASUMarkPercentage + 3.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 73):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "✔");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 75)
                                                {
                                                    this.FindAndReplace(tempApp, "<b9>", "✔");
                                                    this.FindAndReplace(tempApp, "<b10>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b9>", "");
                                                    this.FindAndReplace(tempApp, "<b10>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (4.0 / 3) * ASUMarkPercentage - (124.0 / 3);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 70):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "✔");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 72)
                                                {
                                                    this.FindAndReplace(tempApp, "<b10>", "✔");
                                                    this.FindAndReplace(tempApp, "<b11>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b10>", "");
                                                    this.FindAndReplace(tempApp, "<b11>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 67):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "✔");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 69)
                                                {
                                                    this.FindAndReplace(tempApp, "<b11>", "✔");
                                                    this.FindAndReplace(tempApp, "<b12>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b11>", "");
                                                    this.FindAndReplace(tempApp, "<b12>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = ASUMarkPercentage - 17.0;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 64):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "✔");
                                                this.FindAndReplace(tempApp, "<c11>", "");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 66)
                                                {
                                                    this.FindAndReplace(tempApp, "<b13>", "✔");
                                                    this.FindAndReplace(tempApp, "<b14>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b13>", "");
                                                    this.FindAndReplace(tempApp, "<b14>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox3.Checked == true)
                                                {
                                                    UELMarkPercentage = (5.0 / 3) * ASUMarkPercentage - (185.0 / 3);
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression >= 60):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "✔");
                                                this.FindAndReplace(tempApp, "<c12>", "");

                                                if (expression >= 62)
                                                {
                                                    this.FindAndReplace(tempApp, "<b15>", "✔");
                                                    this.FindAndReplace(tempApp, "<b16>", "");
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<b15>", "");
                                                    this.FindAndReplace(tempApp, "<b16>", "✔");
                                                }
                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b17>", "");
                                                this.FindAndReplace(tempApp, "<b18>", "");
                                                this.FindAndReplace(tempApp, "<b19>", "");
                                                this.FindAndReplace(tempApp, "<b20>", "");

                                                if (checkBox1.Checked == true)
                                                {
                                                    UELMarkPercentage = 1.25 * ASUMarkPercentage - 35;
                                                    UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                    if (checkBox5.Checked)
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                    else
                                                        StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                }
                                                else
                                                {
                                                    StringUELMarkPercentage = " ";
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            case double expression when (expression < 60):
                                                this.FindAndReplace(tempApp, "<c1>", "");
                                                this.FindAndReplace(tempApp, "<c2>", "");
                                                this.FindAndReplace(tempApp, "<c3>", "");
                                                this.FindAndReplace(tempApp, "<c4>", "");
                                                this.FindAndReplace(tempApp, "<c5>", "");
                                                this.FindAndReplace(tempApp, "<c6>", "");
                                                this.FindAndReplace(tempApp, "<c7>", "");
                                                this.FindAndReplace(tempApp, "<c8>", "");
                                                this.FindAndReplace(tempApp, "<c9>", "");
                                                this.FindAndReplace(tempApp, "<c10>", "");
                                                this.FindAndReplace(tempApp, "<c11>", "");

                                                this.FindAndReplace(tempApp, "<b1>", "");
                                                this.FindAndReplace(tempApp, "<b2>", "");
                                                this.FindAndReplace(tempApp, "<b3>", "");
                                                this.FindAndReplace(tempApp, "<b4>", "");
                                                this.FindAndReplace(tempApp, "<b5>", "");
                                                this.FindAndReplace(tempApp, "<b6>", "");
                                                this.FindAndReplace(tempApp, "<b7>", "");
                                                this.FindAndReplace(tempApp, "<b8>", "");
                                                this.FindAndReplace(tempApp, "<b9>", "");
                                                this.FindAndReplace(tempApp, "<b10>", "");
                                                this.FindAndReplace(tempApp, "<b11>", "");
                                                this.FindAndReplace(tempApp, "<b12>", "");
                                                this.FindAndReplace(tempApp, "<b13>", "");
                                                this.FindAndReplace(tempApp, "<b14>", "");
                                                this.FindAndReplace(tempApp, "<b15>", "");
                                                this.FindAndReplace(tempApp, "<b16>", "");

                                                StringUELMarkPercentage = " ";

                                                if (xlWorkSheet.Cells[6, i].Value2 != null && xlWorkSheet.Cells[rowCounter, i].Value2 != null)
                                                {
                                                    this.FindAndReplace(tempApp, "<c12>", "✔");
                                                    if (expression >= 59)
                                                    {
                                                        this.FindAndReplace(tempApp, "<b17>", "✔");
                                                        this.FindAndReplace(tempApp, "<b18>", "");
                                                        this.FindAndReplace(tempApp, "<b19>", "");
                                                        this.FindAndReplace(tempApp, "<b20>", "");
                                                    }
                                                    else if (expression >= 40)
                                                    {
                                                        this.FindAndReplace(tempApp, "<b17>", "");
                                                        this.FindAndReplace(tempApp, "<b18>", "✔");
                                                        this.FindAndReplace(tempApp, "<b19>", "");
                                                        this.FindAndReplace(tempApp, "<b20>", "");
                                                    }
                                                    else if (expression >= 20)
                                                    {
                                                        this.FindAndReplace(tempApp, "<b17>", "");
                                                        this.FindAndReplace(tempApp, "<b18>", "");
                                                        this.FindAndReplace(tempApp, "<b19>", "✔");
                                                        this.FindAndReplace(tempApp, "<b20>", "");
                                                    }
                                                    else
                                                    {
                                                        this.FindAndReplace(tempApp, "<b17>", "");
                                                        this.FindAndReplace(tempApp, "<b18>", "");
                                                        this.FindAndReplace(tempApp, "<b19>", "");
                                                        this.FindAndReplace(tempApp, "<b20>", "✔");
                                                    }
                                                    if (checkBox3.Checked == true)
                                                    {
                                                        UELMarkPercentage = (2.0 / 3) * ASUMarkPercentage;
                                                        UELMarkPercentage = Math.Round(UELMarkPercentage, 2);
                                                        if (checkBox5.Checked)
                                                            StringUELMarkPercentage = UELMarkPercentage.ToString() + "%";
                                                        else
                                                            StringUELMarkPercentage = UELMarkPercentage.ToString();
                                                    }
                                                }
                                                else
                                                {
                                                    this.FindAndReplace(tempApp, "<c12>", "");
                                                    this.FindAndReplace(tempApp, "<b17>", "");
                                                    this.FindAndReplace(tempApp, "<b18>", "");
                                                    this.FindAndReplace(tempApp, "<b19>", "");
                                                    this.FindAndReplace(tempApp, "<b20>", "");
                                                }
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);

                                                break;
                                            default:
                                                StringUELMarkPercentage = " ";
                                                this.FindAndReplace(tempApp, "<UELMark>", StringUELMarkPercentage);
                                                MessageBox.Show("ERROR 404");
                                                xlWorkBook.Close(false, missing, missing);
                                                mytempDoc.Close(false, missing, missing);
                                                tempApp.Quit(false, missing, missing);
                                                myWordDoc.Close(false, missing, missing);
                                                wordApp.Quit(false, missing, missing);
                                                xlApp.Quit();
                                                return;
                                        }
                                    }

                                }
                                else
                                {
                                    MessageBox.Show("File not Found!");
                                }

                                object oPageBreak = WdBreakType.wdPageBreak;
                                if (isFirstTime)
                                {
                                    mytempDoc.SaveAs2(SaveAs1, ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing);
                                    isFirstTime = false;
                                    try
                                    {
                                        object readOnly = false;
                                        wordApp.Visible = false;
                                        mytempDoc.Close();
                                        tempApp.Quit();
                                        myWordDoc = wordApp.Documents.Open(SaveAs1, ref missing, ref readOnly,
                                                           ref missing, ref missing, ref missing,
                                                           ref missing, ref missing, ref missing,
                                                           ref missing, ref missing, ref missing,
                                                           ref missing, ref missing, ref missing, ref missing);
                                        myWordDoc.Activate();
                                    }
                                    catch (Exception error)
                                    {
                                        MessageBox.Show(error.Message + "\n\nTry again after fixing the error.");
                                        xlWorkBook.Close(false, missing, missing);
                                        myWordDoc?.Close(false, missing, missing);
                                        wordApp.Quit(false, missing, missing);
                                        mytempDoc.Close(false, missing, missing);
                                        tempApp.Quit(false, missing, missing);
                                        xlApp.Quit();
                                        return;
                                    }
                                }
                                else
                                {
                                    mytempDoc.SaveAs2(path, ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing,
                                               ref missing, ref missing, ref missing);
                                    mytempDoc.Close();
                                    tempApp.Quit();
                                    wordApp.Selection.Range.InsertBreak(ref oPageBreak);
                                    wordApp.Selection.Range.InsertFile(path, ref missing, ref missing, ref missing, ref missing);
                                    myWordDoc.SaveAs2(ref SaveAs1, ref missing, ref missing, ref missing,
                                                   ref missing, ref missing, ref missing,
                                                   ref missing, ref missing, ref missing,
                                                   ref missing, ref missing, ref missing,
                                                   ref missing, ref missing, ref missing);

                                }
                                if (File.Exists(Path.Combine(path)))
                                {
                                    File.Delete(Path.Combine(path));
                                }
                            }
                            catch
                            {
                                try
                                {
                                    mytempDoc.Close(false, missing, missing);
                                    tempApp.Quit(false, missing, missing);
                                }
                                catch { }

                                try
                                {
                                    myWordDoc.Close(false, missing, missing);
                                    wordApp.Quit(false, missing, missing);
                                }
                                catch { }

                                try
                                {
                                    xlWorkBook.Close(false, missing, missing);
                                    xlApp.Quit();
                                }
                                catch { }
                                return;
                            }
                            progressBar1.Value++;
                        }
                        //Save as
                        try
                        {
                            if (SaveAsType == 1)
                            {
                                myWordDoc.Close();
                                wordApp.Quit();
                            }
                            else if (SaveAsType == 2)
                            {
                                myWordDoc.SaveAs2(ref SaveAs2, Word.WdSaveFormat.wdFormatPDF, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing);
                                myWordDoc.Close();
                                wordApp.Quit();

                                if (File.Exists(Path.Combine(SaveAs1.ToString())))
                                {
                                    File.Delete(Path.Combine(SaveAs1.ToString()));
                                }
                            }
                            else if (SaveAsType == 3)
                            {
                                myWordDoc.SaveAs2(ref SaveAs2, Word.WdSaveFormat.wdFormatPDF, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing,
                                           ref missing, ref missing, ref missing);
                                myWordDoc.Close();
                                wordApp.Quit();
                            }
                        }
                        catch (Exception error)
                        {
                            MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
                            myWordDoc.Close(false, missing, missing);
                            wordApp.Quit(false, missing, missing);
                            return;
                        }
                        rowCounter++;
                    }
                    catch {
                        try
                        {
                            myWordDoc.Close(false, missing, missing);
                            wordApp.Quit(false, missing, missing);
                        }
                        catch {
                            try {
                                wordApp.Quit(false, missing, missing);
                            }
                            catch { }
                        }

                        try
                        {
                            xlWorkBook.Close(false, missing, missing);
                            xlApp.Quit();
                        }
                        catch { }
                    }
                } while ((xlWorkSheet.Cells[rowCounter, 1].Value2 != null) || (xlWorkSheet.Cells[rowCounter, 2].Value2 != null)) ;
                xlWorkBook.Close(false, missing, missing);
                xlApp.Quit();
                progressBar1.Visible = false;
                if (File.Exists(Path.Combine(path)))
                {
                    File.Delete(Path.Combine(path));
                }
            }
            catch (Exception e)
            {
                progressBar1.Visible = false;
                try
                {
                    xlWorkBook.Close(false, missing, missing);
                    xlApp.Quit();
                }
                catch {}

                finally
                {

                    if (e is InvalidBestofQuiz)
                        throw e;
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (checkBox12.Checked && comboBox11.SelectedIndex == 0 && textBox26.Text == "" || checkBox11.Checked && comboBox10.SelectedIndex == 0 && textBox25.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            String Path = textBox8.Text + @"\"+ comboBox16.Text + " - " + comboBox5.Text + " " + dateTimePicker2.Text;
            String NewPath = Path;
            int count1 = 1;
            while (Directory.Exists(NewPath))
            {
                string tempFile = string.Format("{0}({1})", Path, count1++);
                NewPath = tempFile;
            }
            System.IO.Directory.CreateDirectory((NewPath));
            DirectoryInfo dir = new DirectoryInfo(NewPath);
            dir.Attributes |= FileAttributes.Hidden;
            try
            {
                InstructorCreateWordDocumentExcel(NewPath, 1);
                dir.Attributes &= ~FileAttributes.Hidden;
                MessageBox.Show("File Created!");
            }
            catch (Exception error)
            {
                dir.Delete();
                MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
            }

        }

        private void button13_Click(object sender, EventArgs e)
        {
            if (checkBox12.Checked && comboBox11.SelectedIndex == 0 && textBox26.Text == "" || checkBox11.Checked && comboBox10.SelectedIndex == 0 && textBox25.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            String Path = textBox8.Text + @"\"+comboBox16.Text + " - " +comboBox5.Text + " " + dateTimePicker2.Text;
            String NewPath = Path;
            int count1 = 1;
            while (Directory.Exists(NewPath))
            {
                string tempFile = string.Format("{0}({1})", Path, count1++);
                NewPath = tempFile;
            }
            System.IO.Directory.CreateDirectory((NewPath));
            DirectoryInfo dir = new DirectoryInfo(NewPath);
            dir.Attributes |= FileAttributes.Hidden;
            try
            {
                InstructorCreateWordDocumentExcel(NewPath, 2);
                dir.Attributes &= ~FileAttributes.Hidden;
                MessageBox.Show("File Created!");
            }
            catch (Exception error)
            {
                dir.Delete();
                MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (checkBox12.Checked && comboBox11.SelectedIndex == 0 && textBox26.Text == "" || checkBox11.Checked && comboBox10.SelectedIndex == 0 && textBox25.Text == "")
            {
                MessageBox.Show("Please enter signature path first" + "\n\n Try again after fixing the error.");
                return;
            }
            if (textBox8.Text == "")
            {
                ChangePath(textBox8);
                if (textBox8.Text == "") return;
            }
            String Path = textBox8.Text + @"\"+ comboBox16.Text + " - " + comboBox5.Text + " " + dateTimePicker2.Text;
            String NewPath = Path;
            int count1 = 1;
            while (Directory.Exists(NewPath))
            {
                string tempFile = string.Format("{0}({1})", Path, count1++);
                NewPath = tempFile;
            }
            System.IO.Directory.CreateDirectory((NewPath));
            DirectoryInfo dir = new DirectoryInfo(NewPath);
            dir.Attributes |= FileAttributes.Hidden;
            try
            {
                InstructorCreateWordDocumentExcel(NewPath, 3);
                dir.Attributes &= ~FileAttributes.Hidden;
                MessageBox.Show("File Created!");
            }
            catch (Exception error)
            {
                dir.Delete();
                MessageBox.Show(error.Message + "\n\n Try again after fixing the error.");
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.PercentageSymbol = checkBox5.Checked;
            Settings.Default.Save();
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            Settings.Default.BestOfQuizzes = checkBox6.Checked;
            Settings.Default.Save();
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            {
                if (checkBox7.Checked)
                {
                    comboBox6.SelectedIndex = 0;
                    comboBox6.Enabled = true;
                    comboBox6.Visible = true;
                }
                else
                {
                    comboBox6.Enabled = false;
                    comboBox6.Visible = false;
                    textBox5.Visible = false;
                    button14.Enabled = false;
                    button14.Visible = false;
                    textBox21.Visible = false;
                    textBox21.Enabled = false;
                }
                if (comboBox6.SelectedIndex == 0 && checkBox7.Checked)
                {
                    textBox5.Visible = true;
                    button14.Enabled = true;
                    button14.Visible = true;
                    textBox21.Visible = false;
                    textBox21.Enabled = false;
                }
                else if (comboBox6.SelectedIndex == 1 && checkBox7.Checked)
                {
                    textBox5.Visible = false;
                    button14.Enabled = false;
                    button14.Visible = false;
                    textBox21.Visible = true;
                    textBox21.Enabled = true;
                }
                else
                {
                    textBox5.Visible = false;
                    button14.Enabled = false;
                    button14.Visible = false;
                    textBox21.Visible = false;
                    textBox21.Enabled = false;
                }
            }
        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox6.SelectedIndex == 0 && checkBox7.Checked)
            {
                textBox5.Visible = true;
                button14.Enabled = true;
                button14.Visible = true;
                textBox21.Visible = false;
                textBox21.Enabled = false;
            }
            else if (comboBox6.SelectedIndex == 1 && checkBox7.Checked)
            {
                textBox5.Visible = false;
                button14.Enabled = false;
                button14.Visible = false;
                textBox21.Visible = true;
                textBox21.Enabled = true;
            }
            else
            {
                textBox5.Visible = false;
                button14.Enabled = false;
                button14.Visible = false;
                textBox21.Visible = false;
                textBox21.Enabled = false;
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            {
                if (checkBox8.Checked)
                {
                    comboBox7.SelectedIndex = 0;
                    comboBox7.Enabled = true;
                    comboBox7.Visible = true;
                }
                else
                {
                    comboBox7.Enabled = false;
                    comboBox7.Visible = false;
                    textBox16.Visible = false;
                    button15.Enabled = false;
                    button15.Visible = false;
                    textBox22.Visible = false;
                    textBox22.Enabled = false;
                }
                if (comboBox7.SelectedIndex == 0 && checkBox8.Checked)
                {
                    textBox16.Visible = true;
                    button15.Enabled = true;
                    button15.Visible = true;
                    textBox22.Visible = false;
                    textBox22.Enabled = false;
                }
                else if (comboBox7.SelectedIndex == 1 && checkBox8.Checked)
                {
                    textBox16.Visible = false;
                    button15.Enabled = false;
                    button15.Visible = false;
                    textBox22.Visible = true;
                    textBox22.Enabled = true;
                }
                else
                {
                    textBox16.Visible = false;
                    button15.Enabled = false;
                    button15.Visible = false;
                    textBox22.Visible = false;
                    textBox22.Enabled = false;
                }
            }
        }
        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox7.SelectedIndex == 0 && checkBox8.Checked)
            {
                textBox16.Visible = true;
                button15.Enabled = true;
                button15.Visible = true;
                textBox22.Visible = false;
                textBox22.Enabled = false;
            }
            else if (comboBox7.SelectedIndex == 1 && checkBox8.Checked)
            {
                textBox16.Visible = false;
                button15.Enabled = false;
                button15.Visible = false;
                textBox22.Visible = true;
                textBox22.Enabled = true;
            }
            else
            {
                textBox16.Visible = false;
                button15.Enabled = false;
                button15.Visible = false;
                textBox22.Visible = false;
                textBox22.Enabled = false;
            }
        }
        private void button14_Click(object sender, EventArgs e)
        {
            ChangePath2(textBox5);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            ChangePath2(textBox16);
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox10.Checked)
            {
                comboBox9.SelectedIndex = 0;
                comboBox9.Enabled = true;
                comboBox9.Visible = true;
            }
            else
            {
                comboBox9.Enabled = false;
                comboBox9.Visible = false;
                textBox24.Visible = false;
                button17.Enabled = false;
                button17.Visible = false;
                textBox18.Visible = false;
                textBox18.Enabled = false;
            }
            if (comboBox9.SelectedIndex == 0 && checkBox10.Checked)
            {
                textBox24.Visible = true;
                button17.Enabled = true;
                button17.Visible = true;
                textBox18.Visible = false;
                textBox18.Enabled = false;
            }
            else if (comboBox9.SelectedIndex == 1 && checkBox10.Checked)
            {
                textBox24.Visible = false;
                button17.Enabled = false;
                button17.Visible = false;
                textBox18.Visible = true;
                textBox18.Enabled = true;
            }
            else
            {
                textBox24.Visible = false;
                button17.Enabled = false;
                button17.Visible = false;
                textBox18.Visible = false;
                textBox18.Enabled = false;
            }
        }

        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox9.SelectedIndex == 0 && checkBox10.Checked)
            {
                textBox24.Visible = true;
                button17.Enabled = true;
                button17.Visible = true;
                textBox18.Visible = false;
                textBox18.Enabled = false;
            }
            else if (comboBox9.SelectedIndex == 1 && checkBox10.Checked)
            {
                textBox24.Visible = false;
                button17.Enabled = false;
                button17.Visible = false;
                textBox18.Visible = true;
                textBox18.Enabled = true;
            }
            else
            {
                textBox24.Visible = false;
                button17.Enabled = false;
                button17.Visible = false;
                textBox18.Visible = false;
                textBox18.Enabled = false;
            }
        }
        private void button17_Click(object sender, EventArgs e)
        {
            ChangePath2(textBox24);
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox9.Checked)
            {
                comboBox8.SelectedIndex = 0;
                comboBox8.Enabled = true;
                comboBox8.Visible = true;
            }
            else
            {
                comboBox8.Enabled = false;
                comboBox8.Visible = false;
                textBox23.Visible = false;
                button16.Enabled = false;
                button16.Visible = false;
                textBox17.Visible = false;
                textBox17.Enabled = false;
            }
            if (comboBox8.SelectedIndex == 0 && checkBox9.Checked)
            {
                textBox23.Visible = true;
                button16.Enabled = true;
                button16.Visible = true;
                textBox17.Visible = false;
                textBox17.Enabled = false;
            }
            else if (comboBox8.SelectedIndex == 1 && checkBox9.Checked)
            {
                textBox23.Visible = false;
                button16.Enabled = false;
                button16.Visible = false;
                textBox17.Visible = true;
                textBox17.Enabled = true;
            }
            else
            {
                textBox23.Visible = false;
                button16.Enabled = false;
                button16.Visible = false;
                textBox17.Visible = false;
                textBox17.Enabled = false;
            }
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox8.SelectedIndex == 0 && checkBox9.Checked)
            {
                textBox23.Visible = true;
                button16.Enabled = true;
                button16.Visible = true;
                textBox17.Visible = false;
                textBox17.Enabled = false;
            }
            else if (comboBox8.SelectedIndex == 1 && checkBox9.Checked)
            {
                textBox23.Visible = false;
                button16.Enabled = false;
                button16.Visible = false;
                textBox17.Visible = true;
                textBox17.Enabled = true;
            }
            else
            {
                textBox23.Visible = false;
                button16.Enabled = false;
                button16.Visible = false;
                textBox17.Visible = false;
                textBox17.Enabled = false;
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            ChangePath2(textBox23);
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox12.Checked)
            {
                comboBox11.SelectedIndex = 0;
                comboBox11.Enabled = true;
                comboBox11.Visible = true;
            }
            else
            {
                comboBox11.Enabled = false;
                comboBox11.Visible = false;
                textBox26.Visible = false;
                button19.Enabled = false;
                button19.Visible = false;
                textBox20.Visible = false;
                textBox20.Enabled = false;
            }
            if (comboBox11.SelectedIndex == 0 && checkBox12.Checked)
            {
                textBox26.Visible = true;
                button19.Enabled = true;
                button19.Visible = true;
                textBox20.Visible = false;
                textBox20.Enabled = false;
            }
            else if (comboBox11.SelectedIndex == 1 && checkBox12.Checked)
            {
                textBox26.Visible = false;
                button19.Enabled = false;
                button19.Visible = false;
                textBox20.Visible = true;
                textBox20.Enabled = true;
            }
            else
            {
                textBox26.Visible = false;
                button19.Enabled = false;
                button19.Visible = false;
                textBox20.Visible = false;
                textBox20.Enabled = false;
            }
        }

        private void comboBox11_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox11.SelectedIndex == 0 && checkBox12.Checked)
            {
                textBox26.Visible = true;
                button19.Enabled = true;
                button19.Visible = true;
                textBox20.Visible = false;
                textBox20.Enabled = false;
            }
            else if (comboBox11.SelectedIndex == 1 && checkBox12.Checked)
            {
                textBox26.Visible = false;
                button19.Enabled = false;
                button19.Visible = false;
                textBox20.Visible = true;
                textBox20.Enabled = true;
            }
            else
            {
                textBox26.Visible = false;
                button19.Enabled = false;
                button19.Visible = false;
                textBox20.Visible = false;
                textBox20.Enabled = false;
            }
        }

        private void button19_Click(object sender, EventArgs e)
        {
            ChangePath2(textBox26);
        }

        private void checkBox11_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox11.Checked)
            {
                comboBox10.SelectedIndex = 0;
                comboBox10.Enabled = true;
                comboBox10.Visible = true;
            }
            else
            {
                comboBox10.Enabled = false;
                comboBox10.Visible = false;
                textBox25.Visible = false;
                button18.Enabled = false;
                button18.Visible = false;
                textBox19.Visible = false;
                textBox19.Enabled = false;
            }
            if (comboBox10.SelectedIndex == 0 && checkBox11.Checked)
            {
                textBox25.Visible = true;
                button18.Enabled = true;
                button18.Visible = true;
                textBox19.Visible = false;
                textBox19.Enabled = false;
            }
            else if (comboBox10.SelectedIndex == 1 && checkBox11.Checked)
            {
                textBox25.Visible = false;
                button18.Enabled = false;
                button18.Visible = false;
                textBox19.Visible = true;
                textBox19.Enabled = true;
            }
            else
            {
                textBox25.Visible = false;
                button18.Enabled = false;
                button18.Visible = false;
                textBox19.Visible = false;
                textBox19.Enabled = false;
            }
        }

        private void comboBox10_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox10.SelectedIndex == 0 && checkBox11.Checked)
            {
                textBox25.Visible = true;
                button18.Enabled = true;
                button18.Visible = true;
                textBox19.Visible = false;
                textBox19.Enabled = false;
            }
            else if (comboBox10.SelectedIndex == 1 && checkBox11.Checked)
            {
                textBox25.Visible = false;
                button18.Enabled = false;
                button18.Visible = false;
                textBox19.Visible = true;
                textBox19.Enabled = true;
            }
            else
            {
                textBox25.Visible = false;
                button18.Enabled = false;
                button18.Visible = false;
                textBox19.Visible = false;
                textBox19.Enabled = false;
            }
        }

        private void button18_Click(object sender, EventArgs e)
        {
            ChangePath2(textBox25);
        }

        public void ExtractEmbeddedFile(string resName, string fileName)
        {
            if (File.Exists(fileName)) File.Delete(fileName);

            Assembly assembly = Assembly.GetExecutingAssembly();

            using (var input = assembly.GetManifestResourceStream(resName))
            using (var output = File.Open(fileName, FileMode.CreateNew))
            {
                if (input == null) throw new FileNotFoundException(resName + ": Embedded resoure file not found");

                var buffer = new byte[32768];
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    output.Write(buffer, 0, read);
                }
                output.Flush();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (File.Exists(form1Path))
            {
                File.Delete(Path.Combine(form1Path));
            }
            if (File.Exists(form2Path))
            {
                File.Delete(Path.Combine(form2Path));
            }
        }

        private void button20_Click(object sender, EventArgs e)
        {
            if (!File.Exists((string)textBox27.Text))
            {
                StudentPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Student.xlsx");
                ExtractEmbeddedFile("AutoFeedbacks.Student.xlsx", StudentPath);
                textBox27.Text = StudentPath;
            }
            ChangePath(textBox27);
        }

        private void button21_Click(object sender, EventArgs e)
        {
            if (!File.Exists((string)textBox28.Text))
            {
                InstructorPath = System.IO.Path.Combine(System.Windows.Forms.Application.LocalUserAppDataPath, "Instructor.xlsx");
                ExtractEmbeddedFile("AutoFeedbacks.Instructor.xlsx", InstructorPath);
                textBox28.Text = InstructorPath;
            }
            ChangePath(textBox28);
        }

        private void textBox27_TextChanged(object sender, EventArgs e)
        {
            if (!textBox27.Text.EndsWith("Student.xlsx"))
                textBox27.Text = Path.Combine(textBox27.Text, "Student.xlsx");

            if (textBox27.Text != StudentPath || textBox27.Text != Settings.Default["StudentPath"].ToString()) {
                File.Move(StudentPath, textBox27.Text);
                Settings.Default["StudentPath"] = StudentPath = textBox27.Text;
                Settings.Default.Save();
            }
        }

        private void textBox28_TextChanged(object sender, EventArgs e)
        {
            if (!textBox28.Text.EndsWith("Instructor.xlsx"))
                textBox28.Text = Path.Combine(textBox28.Text, "Instructor.xlsx");

            if (textBox28.Text != InstructorPath || textBox28.Text != Settings.Default["InstructorPath"].ToString()) {
                File.Move(InstructorPath, textBox28.Text);
                Settings.Default["InstructorPath"] = InstructorPath = textBox28.Text;
                Settings.Default.Save();
            }
        }

        private void richTextBox1_LinkClicked(object sender, LinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start(e.LinkText);
        }
    }
} 
